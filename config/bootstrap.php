<?php

use DI\ContainerBuilder;
use Slim\App;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

require_once __DIR__ . '/../vendor/autoload.php';

$containerBuilder = new ContainerBuilder();

// Set up settings
$containerBuilder->addDefinitions(__DIR__ . '/container.php');

// Build PHP-DI Container instance
$container = $containerBuilder->build();

// Create App instance
$app = $container->get(App::class);

$app->add(TwigMiddleware::createFromContainer($app, Twig::class));

// Local or Hosting server
$serverAddress = $_SERVER['HTTP_HOST'] ;
if($serverAddress==='mave33.cafe24.com'){
    $app->setBasePath('/mave_griffin');
}
// Register routes
(require __DIR__ . '/routes.php')($app);

// Register middleware
(require __DIR__ . '/middleware.php')($app);

return $app;