<?php

use Psr\Container\ContainerInterface;
use Selective\Config\Configuration;
use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Middleware\ErrorMiddleware;
use Slim\Views\Twig;
use Odan\Twig\TwigAssetsExtension;

error_reporting(1);
ini_set('display_errors', '1');

define('APP_ENV', $_ENV['APP_ENV'] ?? $_SERVER['APP_ENV'] ?? 'DEVELOPMENT');
$settings = (require __DIR__ . '/settings.php')(APP_ENV);

return [
    Configuration::class => function () {
        return new Configuration((require __DIR__ . '/settings.php')(APP_ENV));
    },

    App::class => function (ContainerInterface $container) {
        AppFactory::setContainer($container);
        $app = AppFactory::create();

        // Optional: Set the base path to run the app in a sub-directory
        // The public directory must not be part of the base path
        //$app->setBasePath('/slim4-tutorial');

        return $app;
    },

    ErrorMiddleware::class => function (ContainerInterface $container) {
        $app = $container->get(App::class);
        $settings = $container->get(Configuration::class)->getArray('error_handler_middleware');

        return new ErrorMiddleware(
            $app->getCallableResolver(),
            $app->getResponseFactory(),
            (bool)$settings['display_error_details'],
            (bool)$settings['log_errors'],
            (bool)$settings['log_error_details']
        );
    },

    PDO::class => function (ContainerInterface $container) {
        $settings = $container->get(Configuration::class)->getArray('db');

        $host = $settings['host'];
        $dbname = $settings['database'];
        $username = $settings['username'];
        $password = $settings['password'];
        $charset = $settings['charset'];
        $flags = $settings['flags'];
        $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";

        return new PDO($dsn, $username, $password, $flags);
    },
    \Monolog\Logger::class => function (ContainerInterface $container) {
        $settings = $container->get(Configuration::class)->getArray('logger');

        $logger = new \Monolog\Logger($settings['name']);

        $processor = new \Monolog\Processor\UidProcessor();
        $logger->pushProcessor($processor);

        $handler = new \Monolog\Handler\StreamHandler($settings['path'], $settings['level']);
        $logger->pushHandler($handler);

        $rotatingHandler = new \Monolog\Handler\RotatingFileHandler($settings['path'].$settings['filename'],0,$settings['level'] );
        $rotatingHandler->setFilenameFormat('{date}-{filename}', 'Y-m-d');
        $logger->pushHandler($rotatingHandler);

        $fireHandler = new \Monolog\Handler\FirePHPHandler();
        $logger->pushHandler($fireHandler);
        return $logger;

    },
    Twig::class => function (ContainerInterface $container) {
        $settings = $container->get(Configuration::class);
        $twigSettings = $container->get(Configuration::class)->getArray('twig');
        $assetSettings = $container->get(Configuration::class)->getArray('assets');

        $twig = Twig::create($twigSettings['path'], [
            'cache' => $twigSettings['cache_enabled'] ? $twigSettings['cache_path'] : false,
        ]);

        $loader = $twig->getLoader();
        if ($loader instanceof FilesystemLoader) {
            $loader->addPath($settings['public'], 'public');
        }

        $environment = $twig->getEnvironment();

        // Add Twig extensions
        $twig->addExtension(new TwigAssetsExtension($environment, (array)$assetSettings));

        return $twig;
    }

];