<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

return static function (ContainerBuilder $containerBuilder, array $settings) {
    $containerBuilder->addDefinitions([
        'settings' => $settings,

        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
        \Twig\Environment::class => function (ContainerInterface $container) use ($settings): \Twig\Environment {
            $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../view');
            $twig = new \Twig\Environment($loader, [
                __DIR__ . '/../var/cache'
            ]);
// extra
            $options = $settings['option'];
            $twig->addExtension(new \Oda\Twig\TwigAssetsExtension($twig,$options));

            $publicAssetsCachePath = $settings['assets']['path'];
            $publicCache = new \Odan\Twig\TwigAssetsCache($publicAssetsCachePath);
            $publicCache->clearCache();

            $internalCache = new \Odan\Twig\TwigAssetsCache($settings['twig']['cache_path']);
            $internalCache->clearCache();


            //
            // 20200420: some what can not make it work
            /// tried  not work with webpack_entry_css tag error
            /**
            $basePath = $settings['public'];
            $twig->addExtension(new \Fullpipe\TwigWebpackExtension\WebpackExtension(
//                '/mave_griffin/public/assets/manifest.json',
                __DIR__.'/public/assets/manifest.json',
                $basePath.'/assets/',
                $basePath.'/assets/'
            ));
             */

            if ($settings['app_env'] === 'DEVELOPMENT') {
                $twig->enableDebug();
            }
            return $twig;
        }
    ]);
};
