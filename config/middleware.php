<?php

/**
 * ebfore and after slim applcation to manipulate the request and response object.
 *
 */
declare(strict_types=1);

use Slim\App;

return static function (App $app) {
    // Pare json, form data and xml
    $app->addBodyParsingMiddleware();
    // Add the slim built-in routing middleware
    $app->addRoutingMiddleware();

//    $app->addErrorMiddleware(true, true, true);
    $app->add(\Slim\Middleware\ErrorMiddleware::class);
};
