<?php

declare(strict_types=1);

error_reporting(1);
ini_set('display_errors','1');
use Slim\App;
use Slim\Routing\RouteCollectorProxy;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

return function (App $app) {
    $app->get('/',\App\Action\Home\HomeAction::class)->setName('root');
    $app->get('/branches',\App\Action\Branch\BranchAction::class)->setName('branches');
    $app->get('/branches/All',\App\Action\Branch\GetAllAction::class)->setName('all_branches');
    $app->get('/branches/{id}',\App\Action\Branch\GetOne::class)->setName('branches_id');

    // return swig
    $app->get('/branches/{id}/students',\App\Action\Students\StudentsAction::class);
    $app->get('/branches/{id}/studentsTest',\App\Action\Students\StudentsActionTest::class);

    // return json all student data
    $app->get('/branches/{id}/students/all',\App\Action\Students\GetAllAction::class);

    $app->post('/branches/{id}/students',\App\Action\Students\CreateAction::class);

    $app->get('/branches/{id}/students/{student_id}',\App\Action\Students\GetOne::class);
    $app->get('/branches/{id}/students/{student_idx}/monitor',\App\Action\Monitor\MonitorAction::class);
    $app->get('/branches/{id}/students/{student_idx}/monitorTest',\App\Action\Monitor\MonitorTestAction::class);


    $app->get('/branches/{id}/students/{student_idx}/report/{mh_idx}',\App\Action\Monitor\ReportAction::class);
    $app->get('/branches/{id}/students/{student_idx}/reporttest/{mh_idx}',\App\Action\Monitor\ReportTestAction::class);

//    $app->get('/branches/{id}/students/{student_id}/reports',\App\Action\Home\HomeAction::class);
    $app->get('/login',\App\Action\Login\LoginAction::class)->setName('login');

    $app->get('/monitor',\App\Action\Monitor\MonitorAction::class)->setName('monitor');
    $app->get('/monitortest',\App\Action\Monitor\MonitorTestAction::class)->setName('monitorTest');

    $app->get('/images/{name:[a-zA-Z0-9-]+\.(?|jpg|png)}',function($req, $res, $args ){
        $fileName = $args['name'];
        $file_path = __DIR__."/../view/img/".$fileName;
        if(!file_exists($file_path)){
            // reroute to the error page
        }

        $image = file_get_contents($file_path);
        $res->getBody()->write($image);
        return $res->withHeader('Content-Type', FILEINFO_MIME_TYPE)->withStatus(201);

    });

    $app->post('/mh_log',\App\Action\MeditationHistoryLogAction::class);
    $app->get('/mh_log/{id}/page/{page:[0-9]+}',\App\Action\MediationHistoryLog\GetTen::class);
    $app->get('/mh_log/{id}/time/{time}/page/{page:[0-9]+}',\App\Action\MediationHistoryLog\GetTenRecent::class);
    // starting form this point
    $app->get('/mh_log/{id}',\App\Action\MediationHistoryLog\GetInitialLogAction::class);


    // create iniital mh record
    $app->post('/mh/{student_idx}',\App\Action\MeditationHistory\PostInit::class);
    // create initial mh record with resource idx [1: korean, 2: english, 3: math]
    $app->post('/mh/{student_idx}/resource/{resource_idx}', \App\Action\MeditationHistory\PostStudentResourceAction::class);
    // update FIRE data after completion.


    // get all past results of given student idx in json format.
    $app->get('/mh/{student_idx}/all', \App\Action\MeditationHistory\GetStudentHistoryAction::class);
    // get list of all mh record for students
//    $app->get('/mh/{student_idx}/all',\App\Action\MeditationHistory\PostInit::class);

    $app->post('/mh/{meditation_history_idx}/updateEndAt', \App\Action\MeditationHistory\UpdateEndAtAction::class);
    $app->post('/mh/{meditation_history_idx}/updateStartAt', \App\Action\MeditationHistory\UpdateStartAtAction::class);
    $app->post('/mh/{meditation_history_idx}/data', \App\Action\MeditationHistory\UpdateMhDataAction::class);


    // student
    $app->post('/student',\App\Action\Students\CreateAction::class);


    //admin

    $app->post('/local/admin/login', \App\Action\Admin\LocalAdminLoginAction::class);
    $app->post('/local/admin/update/comport', \App\Action\Admin\LocalUpdateComportAction::class);
    $app->post('/users', \App\Action\UserCreateAction::class)->setName('users');
};
