<?php

declare(strict_types=1);

use Monolog\Logger;

// Error reproting for production
error_reporting(1);
ini_set('display_errors','1');


return static function(string $appEnv) {

    $settings =  [
        'app_env' => $appEnv,
        'root' => dirname(__DIR___),
        // should be set to false in production
        'display_error_details' => true,
        // paratmer is passed to the default errorHandler
        // view in rendered output by enabling the 'displaying ErrorDetails' setting.
        /// for the console. and unit tests we also disable it.
        'log_errors' => true,

       'db' =>[
           'driver'=>'mysql',
           'host' =>'localhost',
           'username'=>'mave33',
            'password'=>'meddic4022!!',
            'charset'=>'utf8mb4',
            'collation'=>'utf8mb4_general_ci',
            'flags'=>[
                //turn off
                PDO::ATTR_PERSISTENT => false,
                // enable exception
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                // emulate perpared statements
                PDO::ATTR_EMULATE_PREPARES => true,
                // Set Default fetch mode to array
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
                //,
                // set character set
               // PDO::MYSQL_ATTR_INIT_COMMAND=> 'SET NAMES utf8mb4 COLLATE utf8mb4_general_ci'
            ],
        ]
    ];

    $settings['root'] = dirname(__DIR__);
    $settings['temp'] =  $settings['root'].'/tmp';
    $settings['public'] = $settings['root'].'/public';

    $settings['logger' ] = [
        'name' => 'slim-app',
        'path' => $settings['root'].'/var/log',
        'filename'=>'app.log',
        'level' => \Monolog\Logger::DEBUG,

    ];
    // Error handling middlware settings
    $settings['error_handler_middleware'] = [
     //should be set to false in production
        'display_error_details' => true,
    ];

    $settings['twig'] =[
//     'path'=>$settings['root'].'/templates',
        'path'=>$settings['root'].'/view',
     'cache_enabled'=>false,
     'cache_path'=>$settings['temp'].'/twig-cache',
    ];

    $settings['assets']=[
     'path'=>$settings['public'].'/cache',
     'url_base_path'=>'cache/'   ,
        'cache_enabled'=>false,
        'cache_path'=>$settings['temp'],
        'cache_name'=>'assets-cache',
        'minify'=>0

    ];

$settings['options']= [
    'path'=>__DIR__."/public/assets/cache",

    'url_base_path'=>'assets/cache',
    'cache-name'=>'assets-cache',
    'cache_lifetime'=>0,
   'minify'=>1
];

    if ($appEnv === 'DEVELOPMENT') {
        // Overrides for development mode
        $settings['di_compilation_path'] = '';
        $settings['display_error_details'] = true;
        $settings['logger']['path'] = __DIR__ . '/../var/log/app.log';
    }


    return $settings;
};
