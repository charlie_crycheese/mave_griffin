<?php


namespace App\Action\Admin;

use App\Service\AdminService;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class LocalUpdateComportAction extends Base
{
    public function __construct(AdminService $service,Logger $logger )
    {
        $this->service = $service;
        $this->logger = $logger;
    }


    public function __invoke(ServerRequestInterface $request, ResponseInterface $response):ResponseInterface
    {
        $data = json_decode($request->getBody(), true);

        $admin_idx = $data['admin_idx'];
        $comport = $data['comport'];

        $this->logger->debug("Action::admin_id[{$admin_idx}], comport [{$comport}]");
        $result = $this->service->updateComport($admin_idx, $comport);

        $response->getBody()->write((string)json_encode($result));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    }

}