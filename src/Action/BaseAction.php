<?php

declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class BaseAction
{

    protected function jsonResponse(ResponseInterface $response, string $status, $message, int $code): ResponseInterface
    {
        $result = [
            'status' => $status,
            'message' => $message,
        ];
        $response->getBody()->write(json_encode($result));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($code);
    }

}
