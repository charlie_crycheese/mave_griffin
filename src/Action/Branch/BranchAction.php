<?php


namespace App\Action\Branch;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Views\Twig;

class BranchAction
{
    private $twig;

    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response):ResponseInterface
    {
//        return $this->twig->render($response, 'home/home.twig');
        return $this->twig->render($response, 'branch/branch.twig');
    }

}