<?php


namespace App\Action\Branch;

use App\Action\BaseAction;
use App\Service\BranchService;
use Monolog\Logger;


class BranchBaseAction extends BaseAction
{
    protected $service;
    protected $logger;

    public function __construct(BranchService $service, Logger $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }
}



