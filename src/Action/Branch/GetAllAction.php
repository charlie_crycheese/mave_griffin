<?php


namespace App\Action\Branch;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Views\Twig;

class GetAllAction extends BranchBaseAction
{

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response):ResponseInterface
    {


        $result = $this->service->getAll();

        return $this->jsonResponse($response, 'success' , $result, 201);

    }

}