<?php


namespace App\Action;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class HomeAction
{
   public function __invoke( ServerRequestInterface $request, ResponseInterface $response ):ResponseInterface
   {
//       $response->getBody()->write('Hello World');
       /*
       $response->getBody()->write(json_encode(['success'=>true]));
       return $response->withHeader('Content-type','application/json');
       */

       // json return

       $result = ['error'=>['message'=>'Validation Failed']];

       $response->getBody()->write(json_encode($result));

       return $response->withHeader('Content-Type', 'application/json')->withStatus(422);

   }
}