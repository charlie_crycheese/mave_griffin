<?php


namespace App\Action\Login;

use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class LoginAccessAction
{

    private $logger;
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function __invoke( ServerRequestInterface $request, ResponseInterface $response)
    {
        $this->logger->debug(__DIR__ . '_starts');
        $user_id = $request['user_id']??null;
        $password = $request['password']??null;


    }
}