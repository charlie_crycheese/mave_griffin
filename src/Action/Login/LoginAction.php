<?php


namespace App\Action\Login;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;
use Slim\Views\Twig;

final class LoginAction
{
    private $twig;

    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response):ResponseInterface
    {
        // TODO: Implement __invoke() method.
//        return $this->twig->render($response, 'home/home.twig');
        return $this->twig->render($response, 'login/login.html.twig');
    }
}