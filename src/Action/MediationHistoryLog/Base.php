<?php
declare(strict_types=1);

namespace App\Action\MediationHistoryLog;

use App\Service\MeditationHistoryLogService;

use Monolog\Logger;

use DI\Container;

class Base extends \App\Action\BaseAction
{
    protected $service ;
    protected $logger;
    public function __construct(MeditationHistoryLogService $service,Logger $logger )
    {
        $this->service = $service;
        $this->logger = $logger;
    }

}