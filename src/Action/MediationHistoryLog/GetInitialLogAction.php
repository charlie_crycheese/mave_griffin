<?php


namespace App\Action\MediationHistoryLog;


use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class GetInitialLogAction extends  Base
{

    public function __invoke(Request $request, Response $response, array $args):Response
    {

        $input = $request->getBody();
        $MeditationHistoryLogId = (int)$args['id'];
        $result = $this->service->getInitialResultForLiveStream($MeditationHistoryLogId);
        $log = ['result' => $result];

        return $this->jsonResponse($response, 'scuccess', $log, 201);
    }
}