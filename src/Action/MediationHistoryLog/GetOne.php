<?php

declare(strict_types=1);
namespace App\Action\MediationHistoryLog;


use Slim\Psr7\Request;
use Slim\Psr7\Response;

class GetOne extends Base
{
    public function __invoke(Request $request, Response $response, array $args):Response
    {

        $input = $request->getBody();
        $MeditationHistoryLogId = (int) $args['id'];
        $this->logger->debug("id[{$MeditationHistoryLogId}]");
        $log = ['result'=>['message'=>$MeditationHistoryLogId]];

        return $this->jsonResponse($response, 'scuccess', $log, 201);
    }
}