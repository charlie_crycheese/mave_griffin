<?php

declare(strict_types=1);
namespace App\Action\MediationHistoryLog;

use  Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\RequestInterface as Request;

class GetTenRecent extends Base
{
    public function __invoke(Request $request, Response $response, array $args):Response
    {

        $input = $request->getBody();
        $MeditationHistoryLogId = (int) $args['id'];
        $page = (int) $args['page'];
        $time = $args['time'];
        $this->logger->debug("id[{$MeditationHistoryLogId}], page[$page]");
        $result = $this->service->getTenRecent($MeditationHistoryLogId,$time, $page);
        $log = ['result'=>$result];


        return $this->jsonResponse($response, 'scuccess', $log, 201);
    }
}