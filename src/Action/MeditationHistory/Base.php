<?php
declare(strict_types=1);

namespace App\Action\MeditationHistory;

use App\Service\MeditationHistoryService;

use Monolog\Logger;

use DI\Container;

class Base extends \App\Action\BaseAction
{
    protected $service ;
    protected $logger;
    public function __construct(MeditationHistoryService $service,Logger $logger )
    {
        $this->service = $service;
        $this->logger = $logger;
    }

}