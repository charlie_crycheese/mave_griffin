<?php
declare(strict_types=1);

namespace App\Action\MeditationHistory;

use App\Service\MeditationHistoryService;

use Monolog\Logger;

use DI\Container;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class  GetStudentHistoryAction extends Base
{
    protected $service ;
    protected $logger;
    public function __construct(MeditationHistoryService $service,Logger $logger )
    {

        $this->service = $service;
        $this->logger = $logger;
    }
    public function __invoke(Request $request, Response $response, array $args):Response{
        $input = $request->getBody();
        $student_idx = (int) $args['student_idx'];
        $this->logger->debug("student_idx[{$student_idx}]");
        //TODO: ERROR Handling required
        $result = ['result'=>$this->service->getListByStudent($student_idx)];

        return $this->jsonResponse($response, 'success', $result, 201);
    }
}