<?php


declare(strict_types=1);
namespace App\Action\MeditationHistory;

use  Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\RequestInterface as Request;

class PostStudentResourceAction extends Base
{
    public function __invoke(Request $request, Response $response, array $args):Response{
        $input = $request->getBody();
        $student_idx = (int) $args['student_idx'];
        $resource_idx = (int) $args['resource_idx'];
        $this->logger->debug("student_idx[{$student_idx}], resource_idx [{$resource_idx}]");
        //TODO: ERROR Handling required
        $result = ['result'=>$this->service->createStudentResource($student_idx, $resource_idx)];

        return $this->jsonResponse($response, 'scuccess', $result, 201);
    }
}