<?php


declare(strict_types=1);

namespace App\Action\MeditationHistory;


use  Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\RequestInterface as Request;

class UpdateEndAtAction extends \App\Action\MeditationHistory\Base
{

    public function __invoke(Request $request, Response $response, array $args):Response{
        $input = $request->getBody();
        $meditation_history_idx = (int) $args['meditation_history_idx'];
        $this->logger->debug("student_idx[{$meditation_history_idx}]");
        //TODO: ERROR Handling required
        $result = ['result'=>$this->service->updateEndAt($meditation_history_idx)];

        return $this->jsonResponse($response, 'scuccess', $result, 201);
    }

}