<?php


declare(strict_types=1);

namespace App\Action\MeditationHistory;


use  Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\RequestInterface as Request;

class UpdateMhDataAction extends \App\Action\MeditationHistory\Base
{

    public function __invoke(Request $request, Response $response, array $args):Response{
//        $data = json_decode($request->getBody(), true);
//        $data = $request->getBody();
        $data = $request->getParsedBody();
        $mh_idx = (int) $args['meditation_history_idx'];
        $this->logger->debug("mh_idx[{$mh_idx}]");
        $this->logger->debug("data " . json_encode($data));
        $this->logger->debug("data count " . count($data));
        //TODO: ERROR Handling required
        $result = ['result'=> $this->service->updateMhData($mh_idx, $data) ];

        return $this->jsonResponse($response, 'scuccess', $result, 201);
    }

}