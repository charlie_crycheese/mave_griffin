<?php

namespace App\Action;


use App\Domain\MeditationHistoryLog\Data\MeditationHistoryLogCreateData;
use App\Domain\MeditationHistoryLog\Service\MeditationHistoryLogService;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class MeditationHistoryLogAction
{
    private $service;
    private $logger;

    public function __construct(MeditationHistoryLogService $service, Logger $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response):ResponseInterface
    {
        // Colllect input from the HTTP request
//         $data = ( $request->getParsedBody());
        $data = json_decode($request->getBody(), true);
//        $string = is_null($data)?'true null':'no';
//        $response->getBody()->write(json_encode((array)$request->getParsedBody(),JSON_THROW_ON_ERROR));

//        $response->getBody()->write(json_encode($data));
//        $response->getBody()->write('\n\\');
//        $response->getBody()->write($data['email']??'false');

//        return $response;

//        $this->logger->debug('data:: ' . json_encode($data));

        $meditationHistoryLog = new MeditationHistoryLogCreateData();
        $meditationHistoryLog->meditation_history_idx = $data['meditation_history_idx'];
        $meditationHistoryLog->fp1_delta = $data['fp1_delta'];
        $meditationHistoryLog->created_at = $data['created_at'];
        $meditationHistoryLog->fp1_theta = $data['fp1_theta'];
        $meditationHistoryLog->fp1_alpha = $data['fp1_alpha'];
        $meditationHistoryLog->fp1_beta = $data['fp1_beta'];
        $meditationHistoryLog->fp1_gamma = $data['fp1_gamma'];
        $meditationHistoryLog->fp2_delta = $data['fp2_delta'];
        $meditationHistoryLog->fpt2_theta = $data['fpt2_theta'];
        $meditationHistoryLog->fp2_beta = $data['fp2_beta'];
        $meditationHistoryLog->fp2_alpha = $data['fp2_alpha'];
        $meditationHistoryLog->fp2_gamma = $data['fp2_gamma'];
        $meditationHistoryLog->sdnn = $data['sdnn'];
        $meditationHistoryLog->rmssd = $data['rmssd'];
        $meditationHistoryLog->bpm = $data['bpm'];
        $meditationHistoryLog->fp1_sq = $data['fp1_sq'];
        $meditationHistoryLog->fp2_sq = $data['fp2_sq'];
        $meditationHistoryLog->attention = $data['attention'];
        $meditationHistoryLog->stress = $data['stress'];

        //Invoke the Domain with inputs and retain the result
        $meditationHistoryLogId = $this->service->createMeditationHistoryLog($meditationHistoryLog);

        $result = [
         'id' => $meditationHistoryLogId
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    }


}

