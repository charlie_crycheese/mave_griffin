<?php


namespace App\Action\Monitor;


use App\Service\MeditationHistoryService;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Views\Twig;

final class MonitorTestAction
{
    private $twig;

    public function __construct(Twig $twig , MeditationHistoryService $mhService, Logger $logger)
    {
        $this->twig = $twig;
        $this->mhService = $mhService;
        $this->logger = $logger;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args):ResponseInterface
    {
        // TODO: Implement __invoke() method.
//        return $this->twig->render($response, 'home/home.twig');

//
        $student_idx = $args['student_idx']??-1;
        $branch_name = $args['id']??'';
        $this->logger->debug("student_idx[{$student_idx}]");

        $mhr = $this->mhService->getMeditationHistoryWithStudent($student_idx);

//        $this->logger->debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".json_encode($mhr));

        $firstrow =$mhr[0];
        $branch_idx = $firstrow['branch_idx'];

        $this->logger->debug("student_name[".$firstrow['student_name']."]");

//        $branch_idx = $args['idx']??-1;

        return $this->twig->render($response, 'monitor/monitor_test.twig'
            , ['branch_name'=>$branch_name
                ,'student_idx'=>$student_idx
                , 'branch_idx' =>$branch_idx
                ,'student_name'=>$firstrow['student_name']
                , 'mh_idx'=>$firstrow['meditation_history_idx']
                , 'created_at'=>$firstrow['created_at']
            ]
        );
    }

}