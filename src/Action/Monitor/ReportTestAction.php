<?php


namespace App\Action\Monitor;


use App\Service\MeditationHistoryService;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Views\Twig;

final class ReportTestAction
{
    private $twig;

    public function __construct(Twig $twig , MeditationHistoryService $mhService, Logger $logger)
    {
        $this->twig = $twig;
        $this->mhService = $mhService;
        $this->logger = $logger;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args):ResponseInterface
    {
        // TODO: Implement __invoke() method.
//        return $this->twig->render($response, 'home/home.twig');

//
        $student_idx = $args['student_idx']??-1;
        $branch_name = $args['id']??'';
        $mh_idx = $args['mh_idx']??-1;


        $baselineMhIdx = $this->mhService->getBaseLineMhIdxByStudentIdx($student_idx);
        $this->logger->debug(__NAMESPACE__.__CLASS__.__FUNCTION__.":baseline".json_encode($baselineMhIdx));

        $baselineData = array();
        if($baselineMhIdx>0){
            $baseLineData = $this->mhService->getAggreagtedMeditationHistoryLogForReport($baselineMhIdx['meditation_history_idx']);
        }

        $mhr = $this->mhService->getMeditationHistoryForReport($mh_idx);
        $aggrMhr = $this->mhService->getAggreagtedMeditationHistoryLogForReport($mh_idx);
        $allMhrLog = $this->mhService->getMeditationHistoryLogsForReport($mh_idx);

        $branch_idx = $mhr[0]['branch_idx'];
        $this->logger->debug("branch_idx[{$branch_idx}]");

        $student_name = $mhr[0]['student_name'];
        $resource_name = $mhr[0]['resource'];
        $this->logger->debug("student_name[{$student_name}]");
        $totalPage = isset($mhr[0]['heartbeat'])?7:5;
        $displayFireData = isset($mhr[0]['heartbeat'])?"":"display:none;";

        return $this->twig->render($response, 'report/reportTest.twig'
            , ['branch_name'=>$branch_name
                , 'branch_idx' =>$branch_idx
                ,'student_name'=>$student_name
                , 'created_at' => substr($mhr[0]['created_at'],0,10)
                , 'resource_name'=>$resource_name
                , 'mh_idx'=>$mh_idx
                , 'mhr'=>$mhr[0]
                , 'aggMhr'=>$aggrMhr
                , 'allMhrLog'=>$allMhrLog
                , 'totalPage'=>$totalPage
                , 'displayFireData' => $displayFireData
                , 'baselineData' => $baseLineData
            ]
        );
    }

}