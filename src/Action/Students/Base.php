<?php
declare(strict_types=1);

namespace App\Action\Students;

use App\Service\StudentService;

use Monolog\Logger;

use DI\Container;

class Base extends \App\Action\BaseAction
{
    protected $service ;
    protected $logger;

    public function __construct(StudentService $service,Logger $logger )
    {
        $this->service = $service;
        $this->logger = $logger;
    }

}