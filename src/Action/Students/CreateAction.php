<?php


namespace App\Action\Students;

use App\Service\StudentService;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CreateAction extends Base
{
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $this->logger->debug(' >>> Student Action  ');
        $this->logger->debug('data ' . json_encode($request->getParsedBody(),true));

        $data = $request->getParsedBody();

        $this->logger->debug("data afetr parsed:".json_encode($data));
        $this->logger->debug("data afetr parsed : ".json_encode($data));
//
        $branch_idx = $data['branch_idx'];
        $student_type = $data['student_type'];
        $student_name = $data['student_name'];
        $phone_number = $data['phone_number'];

        $student_idx = $this->service->create($branch_idx, $student_type, $student_name, $phone_number);

        $result = [
            'id' => $student_idx
        ];

//        $response->getBody()->write((string)json_encode($result));
        return $this->jsonResponse($response, 'success',$result,201);


    }

    public function __construct(StudentService $service, Logger $logger)
    {
        $this->logger = $logger;
        $this->service = $service;
    }

}