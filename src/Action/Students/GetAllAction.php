<?php


namespace App\Action\Students;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Views\Twig;

class GetAllAction extends Base
{

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args):ResponseInterface
    {

        $branch_idx =  $args['id'];

        $result = $this->service->getAll($branch_idx);

        return $this->jsonResponse($response, 'success' , $result, 201);

    }

}