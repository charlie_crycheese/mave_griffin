<?php

declare(strict_types=1);
namespace App\Action\Students;


use App\Service\BranchService;
use App\Service\StudentService;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

class StudentsActionTest extends Base
{
    private $twig;
    private $branchService ;

    /**
     * StudentsAction constructor.
     * @param Twig $twig
     * @param BranchService $branchService
     */
    public function __construct(Twig $twig , BranchService $branchService, Logger $logger, StudentService $service)
    {
        parent::__construct($service, $logger);
        $this->twig = $twig;
        $this->branchService = $branchService;
    }

    public function __invoke(Request $request, Response $response, array $args):Response
    {

        $input = $request->getBody();
        $branch_idx =  $args['id'];
        $branch = $this->branchService->getBranchName($branch_idx);

        return $this->twig->render($response, 'students/students_test.twig',['branch'=>$branch['branch_name'], 'branch_idx'=>$branch_idx] );
    }
}