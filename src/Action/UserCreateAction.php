<?php

namespace App\Action;


use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Service\UserCreator;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UserCreateAction
{
    private $userCreator;
    private $logger;

    public function __construct(UserCreator $creator, Logger $logger)
    {
        $this->userCreator = $creator;
        $this->logger = $logger;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response):ResponseInterface
    {
        // Colllect input from the HTTP request
         $data = ( $request->getParsedBody());
//        $data = json_decode($request->getBody(), true);
//        $string = is_null($data)?'true null':'no';
//        $response->getBody()->write(json_encode((array)$request->getParsedBody(),JSON_THROW_ON_ERROR));

//        $response->getBody()->write(json_encode($data));
//        $response->getBody()->write('\n\\');
//        $response->getBody()->write($data['email']??'false');

//        return $response;

        $this->logger->debug('data ' . json_encode($request));

        $user = new UserCreateData();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $this->logger->debug('user name ' . $user->name);

        //Invoke the Domain with inputs and retain the result
        $userId = $this->userCreator->createUser($user);

        $result = [
         'id' => $userId
        ];

        $response->getBody()->write((string)json_encode($result));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    }
}

