<?php

namespace App\Domain\MeditationHistoryLog\Data;

final class MeditationHistoryLogCreateData
{
    /** @var integer */
    public $meditation_history_idx;
    /** @var float  */
    public $fp1_delta ;
    public $created_at;
    /** @var float  */
    public $fp1_theta;
    /** @var float  */
    public $fp1_alpha;
    /** @var float  */
    public $fp1_beta;
    /** @var float  */
    public $fp1_gamma;
    /** @var float  */
    public $fp2_delta;
    /** @var float  */
    public $fpt2_theta;
    /** @var float  */
    public $fp2_alpha;
    /** @var float  */
    public $fp2_beta;
    /** @var float  */
    public $fp2_gamma;
    /** @var float  */
    public $bpm;
    /** @var float  */
    public $sdnn;
    /** @var float  */
    public $rmssd;

    public $fp1_sq;
    public $fp2_sq;
    public $attention;
    public $stress;
}