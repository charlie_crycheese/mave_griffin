<?php

namespace App\Domain\MeditationHistoryLog\Repository;


use App\Domain\MeditationHistoryLog\Data\MeditationHistoryLogCreateData;

use Monolog\Logger;
use PDO;

/**
 * Repository
 */
class MeditationHistoryLogCreatorRepository
{
    /**
     * @var PDO
     */
    private $connection;
    public $error_message;

    public function __construct(PDO $connection, Logger $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    /**
     * Insert user row
     * @param UserCreateData $user the user
     * @return int the new user id
     */
    public function insertMeditationHistoryLog(MeditationHistoryLogCreateData $log): int
    {
        $row = [
            'meditation_history_idx' => $log->meditation_history_idx,
            'fp1_alpha' => $log->fp1_alpha,
            'fp1_beta' => $log->fp1_beta,
            'fp1_delta' => $log->fp1_delta,
            'fp1_gamma' => $log->fp1_gamma,
            'fp1_theta' => $log->fp1_theta,
            'fp2_alpha' => $log->fp2_alpha,
            'fp2_beta' => $log->fp2_beta,
            'fp2_delta' => $log->fp2_delta,
            'fp2_gamma' => $log->fp2_gamma,
            'fpt2_theta' => $log->fpt2_theta,
            'created_at' => $log->created_at,
            'bpm' => $log->bpm,
            'sdnn' => $log->sdnn,
            'rmssd' => $log->rmssd,
            'fp1_sq' =>$log->fp1_sq,
            'fp2_sq' =>$log->fp2_sq,
            'attention' =>$log->attention,
            'stress' =>$log->stress
        ];

        $sql = "INSERT INTO mave33.meditation_history_log SET
                                             meditation_history_idx = :meditation_history_idx
                                           , created_at = from_unixtime(:created_at)
                                           , fp1_alpha = :fp1_alpha
                                           , fp1_beta = :fp1_beta
                                           , fp1_delta = :fp1_delta
                                           , fp1_gamma = :fp1_gamma
                                           , fp1_theta = :fp1_theta
                                           , fp2_alpha = :fp2_alpha
                                           , fp2_beta = :fp2_beta
                                           , fp2_delta = :fp2_delta
                                           , fp2_gamma = :fp2_gamma
                                           , fpt2_theta = :fpt2_theta 
                                           , bpm = :bpm
                                           , sdnn = :sdnn
                                           , rmssd = :rmssd
                                           , fp1_sq = :fp1_sq
                                            ,fp2_sq =:fp2_sq
                                            , attention =:attention
                                            , stress =:stress

                                            ;
                                            ";

        $stmt = $this->connection->prepare($sql);
        if (!$stmt) {
            $this->error_message = $stmt->errorInfo();
        }

        if ($stmt && $stmt->execute($row)) {
            return (int)$this->connection->lastInsertId();
        } else {
            $this->logger->debug("ERRROR >><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            return false;
        }
    }

}