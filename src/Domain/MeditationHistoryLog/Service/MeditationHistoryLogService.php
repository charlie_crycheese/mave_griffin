<?php

namespace App\Domain\MeditationHistoryLog\Service;

use App\Domain\MeditationHistoryLog\Data\MeditationHistoryLogCreateData;
use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Repository\UserCreatorRepository;
use InvalidArgumentException;
use Monolog\Logger;

/**
 * Service
 *
 */
final class MeditationHistoryLogService
{
    private $repository;

    /**
     * UserCreator constructor.
     * @param  $repository
     */
    public function __construct(\App\Domain\MeditationHistoryLog\Repository\MeditationHistoryLogCreatorRepository $repository, Logger $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    /**
     * create a new user.
     * @param UserCreateData $user the user data
     * @return int the new user id
     * @throws InvalidArgumentException
     */
    public function createMeditationHistoryLog(MeditationHistoryLogCreateData $user): int
    {
        if (empty($user->meditation_history_idx)) {
            throw new InvalidArgumentException('Meditation hisotry idx  required');
        }

        // Insert user
        try {
            $meditationHistoryLogId = $this->repository->insertMeditationHistoryLog($user);
        } catch (\Exception $exception) {
           $this->logger->error($exception->getMessage());
           return "DB ERROR";
        }
        //Loggin here : User created successfully
        return $meditationHistoryLogId;
    }
}


