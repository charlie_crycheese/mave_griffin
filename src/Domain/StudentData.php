<?php


namespace App\Domain;


final class StudentData
{
    /**
     * @var  int
     */
    public $student_idx;

    /**
     * @var  string: 중학생, 고등학생, 일반인
     */
    public $student_type;

    /**
     * @var  string
     */
    public $student_name;

    /**
     * @var  int
     */
    public $branch_idx;

    /**
     * @var  datetime
     */
    public $created_at;

    /**
     * @var  timestamp
     */
    public $updated_at;

}