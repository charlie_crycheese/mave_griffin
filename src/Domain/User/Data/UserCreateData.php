<?php

namespace App\Domain\User\Data;

final class UserCreateData
{
    /** @var string */
    public $name;
    /** @var string  */
    public $email ;
}