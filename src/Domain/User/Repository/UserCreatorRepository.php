<?php
namespace App\Domain\User\Repository;


use App\Domain\User\Data\UserCreateData;
use PDO;

/**
 * Repository
 */
class UserCreatorRepository
{
    /**
     * @var PDO
     */
    private $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert user row
     * @param UserCreateData $user the user
     * @return int the new user id
     */
    public function insertUser(UserCreateData $user):int
    {
        $row = [
            'name'=>$user->name,
            'email'=>$user->email
        ];

        $sql = "INSERT INTO mave33.users SET name = :name , email = :name";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }

}