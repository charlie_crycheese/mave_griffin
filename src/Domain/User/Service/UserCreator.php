<?php

namespace App\Domain\User\Service;

use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Repository\UserCreatorRepository;
use InvalidArgumentException;

/**
 * Service
 *
 */

final class UserCreator
{
    private $repository;

    /**
     * UserCreator constructor.
     * @param UserCreatorRepository $repository
     */
    public function __construct(\App\Domain\User\Repository\UserCreatorRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * create a new user.
     * @param UserCreateData $user the user data
     * @throws I\\nvalidArgumentException
     * @return int the new user id
     */
    public function createUser( UserCreateData $user ):int
    {
        if(empty($user->name)){
            throw new InvalidArgumentException('UserName required');
        }

        // Insert user
        $userId = $this->repository->insertUser($user);

        //Loggin here : User created successfully

        return $userId;
    }
}


