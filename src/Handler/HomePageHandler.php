<?php

declare(strict_types=1);

namespace App\Handler;

use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Response;

class HomePageHandler implements RequestHandlerInterface
{
    private $logger;
    private $twig;

    public function __construct(Logger $logger, \Twig\Environment $twig)
    {
        $this->logger = $logger;
        $this->twig = $twig;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->logger->info('Home page handler dispatched');

        $name = $request->getAttribute('name', 'CHARLIE');
        $response = new Response();
//        $response->getBody()->write("Hello $name");
        $response->getBody()->write(
            $this->twig->render('home-page.twig',['name'=>$name])
        );
        return $response;
    }
}
