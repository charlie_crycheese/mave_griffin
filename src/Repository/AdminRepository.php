<?php


declare(strict_types=1);
namespace App\Repository;
use PDO;

use Monolog\Logger;

class AdminRepository extends BaseRepository
{

    private $connection ;
    private $logger;

    public function __construct(\PDO $conn, Logger $logger){
        $this->connection = $conn;
        $this->logger = $logger;
    }

    public function getAdminInfo($admin_id){
        $query = "
        SELECT  admin_id, admin_idx, password, a.branch_idx  , comport , admin_name
        , branch_name
        FROM mave33.admin a
        INNER JOIN mave33.branch b
        on a.branch_idx = b.branch_idx
        WHERE  admin_id = ?
        ";
        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(1, $admin_id, \PDO::PARAM_STR);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public function updateComport($admin_idx, $comport)
    {
        $query = "
        UPDATE mave33.admin
        SET comport =:comport 
        WHERE admin_idx  = :admin_idx;
        ";
        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':admin_idx', $admin_idx);
        $stmt->bindParam(":comport", $comport);

        if( $stmt && $stmt->execute()){
            $rowcount = $stmt->rowCount();
            $this->logger->debug("row number ".$rowcount);
            return (int) $stmt->rowCount();
        }else{
            $this->logger->error('errror');
            $this->logger->debug("ERRROR : ".$this->connection->errorInfo());
            $this->error_message = $this->connection->errorInfo();
            return 0;
        }

    }

}