<?php

declare(strict_types=1);

namespace App\Repository;


class BranchRepository extends BaseRepository
{
    public function __construct(\PDO $database)
    {
        $this->database = $database;
    }

    public function getAll():array{
        // possible query condition and page
        $query = "
        SELECT * 
        FROM `mave33`.branch
        ";

       $stmt = $this->database->prepare($query);
       $stmt->execute();

       $result = $stmt->fetchAll();

       return $result;
    }

    public function getName($idx):array{
        $query = "
        SELECT  branch_name
         FROM `mave33`.`branch`
        WHERE 1=1 AND branch_idx = ?
        LIMIT 1;
        ";
        $stmt = $this->database->prepare($query);
        $stmt->bindParam( 1, $idx,\PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetch();

        return $result;

    }
}