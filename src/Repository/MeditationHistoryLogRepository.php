<?php
declare(strict_types=1);

namespace App\Repository;


class MeditationHistoryLogRepository extends BaseRepository
{
    public function __construct(\PDO $database)
    {
        $this->database = $database;
    }

    public function getTenResult(int $meditationHistoryLogIdx ,int $page ):array
    {

        $offset = $page * 10;
        $query ="
        SELECT * 
        FROM `mave33`.`meditation_history_log` 
        WHERE 1=1 
         AND meditation_history_idx = :idx
         LIMIT :page, 10 
        ";

        $stmt = $this->database->prepare($query);
        $stmt->bindParam('idx', $meditationHistoryLogIdx);
        $stmt->bindParam('page',$offset, \PDO::PARAM_INT) ;
        $stmt->execute();

        $meditationHistoryLog = $stmt->fetchAll();

        // throws error
//        if(!$meditationHistoryLog)
//        {
//            throw new MeditatinoHistoryLogException('No Notes');
//        }

        return $meditationHistoryLog;
    }

    public function getTenRecent( int $mhl_idx , $startTime,   int $page): array
   {

       $offset = $page * 10;
       $query = "
        SELECT * 
        FROM `mave33`.`meditation_history_log` 
        WHERE 1=1 
        --   AND created_at  > :start_time
         AND meditation_history_idx = :mhl_idx
         LIMIT :page, 10 
        ";

       $stmt = $this->database->prepare($query);
       $stmt->bindParam('mhl_idx', $mhl_idx);
//       $stmt->bindParam('start_time', $startTime);
       $stmt->bindParam('page',$offset, \PDO::PARAM_INT) ;
       $stmt->execute();
       $result = $stmt->fetchAll();

       return $result;
   }

    /**
     * 데이타를 가지고오는 데 시간이 걸리므로 첫 30초전에 들어온 데이타를 초기 데이터를 로딩싱 가지고 온다.
     */
   public function getInitialResultForLiveStream (int $mhl_idx ):array{
        $query = "
        SELECT * 
        FROM `mave33`.meditation_history_log
        WHERE 1=1 
          AND created_at  < DATE_SUB( NOW() , INTERVAL 50 SECOND )
          AND meditation_history_idx = :mhl_idx ; 
        ;
        ";
       $stmt = $this->database->prepare($query);
       $stmt->bindParam('mhl_idx', $mhl_idx);
       $stmt->execute();
       $result = $stmt->fetchAll();

       return $result;
   }




}