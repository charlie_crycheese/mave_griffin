<?php
declare(strict_types=1);

namespace App\Repository;

use Monolog\Logger;
use PDO;

class MeditationHistoryRepository extends BaseRepository
{
    private $connection;
    public $error_message;
    private $logger;

    public function __construct(PDO $connection, Logger $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    public function create($student_idx): int
    {
        $row = ['student_idx' => $student_idx];
        $query = "
        INSERT INTO mave33.meditation_history  
          SET student_idx = :student_idx
            , created_at= NOW() ; 
        ";
        $stmt = $this->connection->prepare($query);

        if (!$stmt) {
            $this->error_message = $this->connection->errorInfo();
        }

        if ($stmt && $stmt->execute($row)) {
            return (int)$this->connection->lastInsertId();
        } else {
            $this->logger->debug("ERRROR : " . $this->connection->errorInfo());
            $this->error_message = $this->connection->errorInfo();
            return 0;
        }
    }

    public function createWithStudentResource($student_idx, $resource_idx)
    {
        $row = ['student_idx' => $student_idx, 'resource_idx' => $resource_idx];

        $query = "
        INSERT INTO mave33.meditation_history  
          SET student_idx = :student_idx
            , resource_idx = :resource_idx
            , created_at= NOW() ; 
        ";
        $stmt = $this->connection->prepare($query);

        if (!$stmt) {
            $this->error_message = $this->connection->errorInfo();
        }

        if ($stmt && $stmt->execute($row)) {
            return (int)$this->connection->lastInsertId();
        } else {
            $this->logger->debug("ERRROR : " . $this->connection->errorInfo());
            $this->error_message = $this->connection->errorInfo();
            return 0;
        }
    }

    /**
     * @param $student_idx
     * @return array
     */
    public function getMostRecentHistoryIdx($student_idx): array
    {
        $query = "
       SELECT s.student_idx
       , s.student_name
       , s.student_type
       , s.branch_idx
       , mh.meditation_history_idx
       , mh.created_at
       , mhl.meditation_history_idx
       , mhl.created_at as mhlcreatedat
       , mhl.last_inserted_date
       FROM `mave33`.meditation_history mh 
       INNER JOIN `mave33`.student s 
       ON mh.student_idx = s.student_idx
       LEFT JOIN ( 
           SELECT meditation_history_idx
                , MIN(created_at) as created_at, max( created_at) as last_inserted_date
       FROM mave33.meditation_history_log
          GROUP BY  meditation_history_idx
           ) as mhl
       ON mhl.meditation_history_idx = mh.meditation_history_idx
      WHERE 1=1  
        AND mh.heartbeat is null 
         -- AND  DATE(NOW())  = DATE(mh.created_at)  /**  오늘 한 것만 할 떄  이 구절이 들어간다. **/
         AND mh.student_idx = ? 
        ORDER BY mh.created_at  DESC
        LIMIT 1 
       ";
        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(1, $student_idx);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * 현재 날짜가 그대로 기록을 한다. 그래서 날짜 컬럼인
     * @param $meditation_history_idx
     * @param $columnName
     * @return int
     */
    public function updateDateColumn($meditation_history_idx, $columnName)
    {
        $query = "
        UPDATE mave33.meditation_history
         SET {$columnName}  = NOW()
        WHERE meditation_history_idx = ? 
        ";
        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(1, $meditation_history_idx);
        if ($stmt && $stmt->execute()) {
            return (int)$stmt->rowCount();
        } else {
            $this->logger->debug("ERRROR : " . $this->connection->errorInfo());
            $this->error_message = $this->connection->errorInfo();
            return 0;
        }
    }

    /**
     * 학생 아이디에 따른 회차 정보를 가지고 온다.
     * @param $student_idx
     */
    public function getMeditationHistoryByStudent($student_idx)
    {
        $query = "
       SELECT mh.meditation_history_idx,  mh.student_idx, mh.created_at, mh.end_at 
       , case 
           when mh.resource_idx =0 THEN '기준측정'
           when mh.resource_idx =1 THEN '국어'
           when mh.resource_idx =2 THEN '영어'
           ELSE '수학' END AS 'resource'
        FROM mave33.meditation_history mh
        WHERE mh.student_idx = ? 
       AND resource_idx is not null 
       AND resource_idx <> 0
      --  AND mh.ans_balance_pct is not null  
       ORDER BY  mh.meditation_history_idx
       ";
        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(1, $student_idx);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * mh_idx를 받아서 해당 보고서 작성을 위한 데이터 정보를 가지고 온다.
     * @param $mh_idx
     */
    public function getMeditationHistoryLogsForReport($mh_idx)
    {
        $query = "
        SELECT 
             @row_num:=@row_num+1 as 'minute'
               ,gr.hourmin
             ,  truncate(avg(gr.fp1_delta),2) as fp1_delta_avg
             , truncate(avg(gr.fp2_delta),2) as fp2_delta_avg
             , truncate((avg(gr.fp1_delta)+avg(gr.fp2_delta) )/2 ,2) AS delta_avg
             , truncate( avg(gr.fp1_theta),2) as fp1_theta_avg
             , truncate(avg(gr.fpt2_theta),2) as fp2_theta_avg
             , truncate((avg(gr.fp1_theta)+avg(gr.fpt2_theta) )/2,2) AS theta_avg
             , truncate(avg(gr.fp1_alpha),2) as fp1_alpha_avg
             , truncate(avg(gr.fp2_alpha),2) as fp2_alpha_avg
             , truncate((avg(gr.fp1_alpha)+avg(gr.fp2_alpha) )/2,2) AS alpha_avg
             , truncate(avg(gr.fp1_beta),2) as fp1_beta_avg
             , truncate(avg(gr.fp2_beta),2) as fp2_beta_avg
             , truncate( (avg(gr.fp1_beta)+avg(gr.fp2_beta) )/2,2) AS beta_avg
             , truncate( avg(gr.fp1_gamma) ,2) as fp1_gamma_avg
             , truncate( avg(gr.fp2_gamma),2) as fp2_gamma_avg
             , truncate((avg(gr.fp1_gamma)+avg(gr.fp2_gamma) )/2,2) AS gamma_avg
             , truncate(avg(gr.bpm) ,2)as bpm_avg
             , truncate(avg(gr.sdnn) ,2)as sdnn_avg
             , truncate(avg(gr.rmssd),2) as rmssd_avg
             , truncate(avg(gr.attention),2)  as attention_avg
             , truncate(avg(gr.stress),2)  as stress_avg
        
        FROM (
                SELECT  EXTRACT( HOUR_MINUTE FROM mhl.created_at ) as 'hourmin'
                , mhl.fp1_delta, mhl.fp2_delta  
                , mhl.fp1_theta, mhl.fpt2_theta
                , mhl.fp1_alpha, mhl.fp2_alpha
                , mhl.fp1_beta, mhl.fp2_beta
               , mhl.fp1_gamma, mhl.fp2_gamma 
                , mhl.bpm
                , mhl.sdnn
                , mhl.rmssd
               , mhl.attention 
                , mhl.stress
                FROM mave33.meditation_history_log mhl
                WHERE mhl.meditation_history_idx = ? 
        ) AS gr
        , ( SELECT @row_num :=0 ) rownumtmp
        GROUP BY  gr.hourmin
        LIMIT 15
        ";

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(1, $mh_idx);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * 상세값에서 통계값을 계산해서 현재 값을 가지고 온다.
     * 최대, 최소, 평균 값을 가지고 온다.
     * @param $mh_idx
     * @return array
     */
    public function getAggreagtedMeditationHistoryLogForReport($mh_idx)
    {
        $query = "
        SELECT minute 
        , truncate(avg(fp1_delta_avg ),2)   as avg_fp1_delta
        , truncate(avg(fp2_delta_avg),2) as avg_fp2_delta
        , truncate(avg(delta_avg),2) as avg_delta
        , truncate(max( fp1_delta_avg) ,2)as max_fp1_delta
        , truncate(max( fp2_delta_avg),2) as max_fp2_delta
        , truncate(min( fp1_delta_avg) ,2)as min_fp1_delta
        , truncate(min( fp2_delta_avg),2) as min_fp2_delta
             
        , truncate(avg( fp1_theta_avg ),2)   as avg_fp1_theta
        , truncate(avg(fp2_theta_avg),2) as avg_fp2_theta
        , truncate(avg(theta_avg),2) as avg_theta
        , truncate(max( fp1_theta_avg) ,2)as max_fp1_theta
        , truncate(max( fp2_theta_avg),2) as max_fp2_theta
        , truncate(min( fp1_theta_avg) ,2)as min_fp1_theta
        , truncate(min( fp2_theta_avg),2) as min_fp2_theta
             
        , truncate(avg( fp1_alpha_avg ),2)   as avg_fp1_alpha
        , truncate(avg(fp2_alpha_avg),2) as avg_fp2_alpha
        , truncate(avg(alpha_avg),2) as avg_alpha
        , truncate(max( fp1_alpha_avg) ,2)as max_fp1_alpha
        , truncate(max( fp2_alpha_avg),2) as max_fp2_alpha
        , truncate(min( fp1_alpha_avg) ,2)as min_fp1_alpha
        , truncate(min( fp2_alpha_avg),2) as min_fp2_alpha
             
              , truncate(avg( fp1_beta_avg ),2)   as avg_fp1_beta
        , truncate(avg(fp2_beta_avg),2) as avg_fp2_beta
        , truncate(avg(beta_avg),2) as avg_beta
        , truncate(max( fp1_beta_avg) ,2)as max_fp1_beta
        , truncate(max( fp2_beta_avg),2) as max_fp2_beta
        , truncate(min( fp1_beta_avg) ,2)as min_fp1_beta
        , truncate(min( fp2_beta_avg),2) as min_fp2_beta
             
              , truncate(avg( fp1_gamma_avg ),2)   as avg_fp1_gamma
        , truncate(avg(fp2_gamma_avg),2) as avg_fp2_gamma
        , truncate(avg(gamma_avg),2) as avg_gamma
        , truncate(max( fp1_gamma_avg) ,2)as max_fp1_gamma
        , truncate(max( fp2_gamma_avg),2) as max_fp2_gamma
        , truncate(min( fp1_gamma_avg) ,2)as min_fp1_gamma
        , truncate(min( fp2_gamma_avg),2) as min_fp2_gamma
        
              , truncate(avg(bpm_avg ),2)   as avg_bpm
              , truncate(max(bpm_avg ),2)   as max_bpm
              , truncate(min(bpm_avg ),2)   as min_bpm
              , truncate(std(bpm_avg ),2)   as std_bpm
        
              , truncate(avg(sdnn_avg ),2)   as avg_sdnn
              , truncate(max(sdnn_avg ),2)   as max_sdnn
              , truncate(min(sdnn_avg ),2)   as min_sdnn
             
              , truncate(avg(rmssd_avg ),2)   as avg_rmssd
              , truncate(max(rmssd_avg ),2)   as max_rmssd
              , truncate(min(rmssd_avg ),2)   as min_rmssd
             
              , truncate(avg(attention_avg ),2)   as avg_attention
              , truncate(max(attention_avg ),2)   as max_attention
              , truncate(min(attention_avg ),2)   as min_attention
        
              , truncate(avg(stress_avg ),2)   as avg_stress
              , truncate(max(stress_avg ),2)   as max_stress
              , truncate(min(stress_avg ),2)   as min_stress
        FROM (
                 SELECT 
                     @row_num:=@row_num+1 as 'minute'
                       ,gr.hourmin
                     ,  avg(gr.fp1_delta) as fp1_delta_avg, avg(gr.fp2_delta) as fp2_delta_avg, (avg(gr.fp1_delta)+avg(gr.fp2_delta) )/2 AS delta_avg
                     ,  avg(gr.fp1_theta) as fp1_theta_avg, avg(gr.fpt2_theta) as fp2_theta_avg, (avg(gr.fp1_theta)+avg(gr.fpt2_theta) )/2 AS theta_avg
                     ,  avg(gr.fp1_alpha) as fp1_alpha_avg, avg(gr.fp2_alpha) as fp2_alpha_avg, (avg(gr.fp1_alpha)+avg(gr.fp2_alpha) )/2 AS alpha_avg
                     ,  avg(gr.fp1_beta) as fp1_beta_avg, avg(gr.fp2_beta) as fp2_beta_avg, (avg(gr.fp1_beta)+avg(gr.fp2_beta) )/2 AS beta_avg
                     ,  avg(gr.fp1_gamma) as fp1_gamma_avg, avg(gr.fp2_gamma) as fp2_gamma_avg, (avg(gr.fp1_gamma)+avg(gr.fp2_gamma) )/2 AS gamma_avg
                     ,  avg(gr.bpm) as bpm_avg
                     ,  avg(gr.sdnn) as sdnn_avg
                     ,  avg(gr.rmssd) as rmssd_avg
                     ,  avg(gr.attention) as attention_avg
                     ,  avg(gr.stress) as stress_avg
                FROM (
                        SELECT  EXTRACT( HOUR_MINUTE FROM mhl.created_at ) as 'hourmin'
                        , mhl.fp1_delta, mhl.fp2_delta  
                        , mhl.fp1_theta, mhl.fpt2_theta
                        , mhl.fp1_alpha, mhl.fp2_alpha
                        , mhl.fp1_beta, mhl.fp2_beta
                       , mhl.fp1_gamma, mhl.fp2_gamma 
                        , mhl.bpm
                        , mhl.sdnn
                        , mhl.rmssd
                        , mhl.attention
                        ,mhl.stress
                        
                        FROM mave33.meditation_history_log mhl
                        WHERE mhl.meditation_history_idx = ? 
                ) AS gr
                , ( SELECT @row_num :=0 ) rownumtmp
                GROUP BY  gr.hourmin
                LIMIT 15
        ) tt 
       ";

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(1, $mh_idx);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }


    /**
     * 헤당 mh_idx 에서 메이브에서 보내져온 정보를 기준측정, 국어 영어, 수학별로 가지고 온다.
     * @param $mh_idx
     * @return array
     */
    public function getMeditationHistoryForReport($mh_idx)
    {
        $query = "
        SELECT st.student_idx, st.student_name, st.student_type
             , st.branch_idx
        , mh.created_at
        , mh.end_at
       , case 
           when mh.resource_idx =0 THEN '기준측정'
           when mh.resource_idx =1 THEN '국어'
           when mh.resource_idx =2 THEN '영어'
           ELSE '수학' END AS 'resource'
        , mh.ans_balance_pct
        , mh.anxiety_pct
        , mh.depression_pct
        , mh.heartbeat
        , mh.hf_pct
        , mh.hf_power
        , mh.lf_pct
        , mh.lf_power
         , mh.heartbeat
         , mh.lr_brain_balance_pct
         , mh.rmssd
         , mh.sdnn
         , mh.pnn50
         , mh.pnn50_pct
         , mh.lf_hf
         , mh.total_power
         , mh.rr
         , mh.std_hr
         , mh.high_hr
         , mh.low_hr
         , mh.vlf_pct
        , mh.vlf_power
         FROM mave33.meditation_history mh 
        INNER JOIN mave33.student st 
        ON mh.student_idx = st.student_idx
        WHERE 1=1
            AND mh.meditation_history_idx  = ? 
            -- AND mh.ans_balance_pct is not null;  
        ";
        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(1, $mh_idx);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * mave 기록이 끝났을 때, 이미 생성된 mh_idx 로 업데이트를 한다.
     * @param $mhData array object
     * @return int
     */
    public function updateMh($mh_idx, $mhData)
    {
        $query = "
      UPDATE mave33.meditation_history 
       SET 
           end_at = NOW()
        , heartbeat=:heartbeat
        , ans_balance_pct=:ans_balance_pct
        , anxiety_pct=:anxiety_pct
        , depression_pct=:depression_pct
        , lr_brain_balance_pct=:lr_brain_balance_pct
		, rmssd=:rmssd
		, sdnn=:sdnn
		, pnn50=:pnn50
		, pnn50_pct=:pnn50_pct
		, lf_hf=:lf_hf
		, total_power=:total_power
		, rr=:rr
		, std_hr=:std_hr
		, high_hr=:high_hr
		, low_hr=:low_hr
		, vlf_pct=:vlf_pct
		, lf_pct=:lf_pct
		, hf_pct=:hf_pct
		, vlf_power=:vlf_power
		, lf_power=:lf_power
		, hf_power=:hf_power
        WHERE meditation_history_idx = :meditation_history_idx
      ";

        $stmt = $this->connection->prepare($query);
        if (!$stmt) {
            $this->error_message = $this->connection->errorInfo();
        }
//        $mhData['meditation_history_idx'] = $mh_idx;
        $this->logger->debug("IS object:" . is_object($mhData));
        $this->logger->debug("IS array:" . is_array($mhData));
        $this->logger->debug("IS string:" . is_string($mhData));

//        $arrayData = json_decode( json_encode( $mhData),true);
//        $arrayData['meditation_history_idx'] = $mh_idx;

        $this->logger->debug("mh datacount >>>>>>>>>>>>>>>>>>heart beat >>>>>>>>>" . PHP_EOL . ($mhData->heartbeat));
        $this->logger->debug("mh COUNT " . PHP_EOL . (count($mhData)));


//        $this->logger->debug("ARRAYDATA count>>>>>>>>>".PHP_EOL.(count($arrayData)));
        $mhDataArray = (array)($mhData);
        $this->logger->debug("mh COUNT  Array " . PHP_EOL . (count($mhDataArray)));
        foreach ($mhDataArray as $key => $value) {
            $this->logger->debug("[{$key}]=>{$value}");
        }

        $this->logger->debug(">>>>>>>>>>>>mH DATA heart :::: " . $mhDataArray['heartbeat']);
//        $this->logger->debug("ARRAYDATA>>>>>>>>>".PHP_EOL.(json_encode($arrayData)));

        if ($stmt && $stmt->execute($mhDataArray)) {
            return (int)$stmt->rowCount();
        } else {
            $this->logger->debug("ERRROR : " . $this->connection->errorInfo());
            $this->error_message = $this->connection->errorInfo();
            return 0;
        }
    }

    /**
     * 가장 최근에 기준측정값을 가지고 온다.
     * @param $student_idx
     * @return int|mixed
     */
    public function getMostRecentBaseLineMhIdxByStudentIdx($student_idx)
    {
        $query = "
        SELECT  meditation_history_idx 
         FROM mave33.meditation_history
        WHERE 1=1  
               AND student_idx = ? --  :student_idx
        AND resource_idx = 0
        AND heartbeat is not null 
        ORDER BY  meditation_history_idx desc    
       LIMIT 1 ; 
        ";
        $this->logger->debug($query);
        $this->logger->debug("STUDENT _IDX ".$student_idx);

        $stmt = $this->connection->prepare($query);
        if (
            $stmt->bindParam(1, $student_idx)
            && $stmt->execute()
        ) {
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0) {
                return $stmt->fetch();
            } else return $rowCount;
        } else {
            $this->logger->error('ERROR' . $this->connection->errorInfo());
            $this->error_message = ($this->connection->errorInfo());
            return 0;
        }
    }

}