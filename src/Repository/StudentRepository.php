<?php

declare(strict_types=1);

namespace App\Repository;

use PDO;

class StudentRepository extends BaseRepository
{

    private $connection;

    public function __construct(\PDO $conn)
    {
        $this->connection = $conn;
    }

    public function getAll($branch_idx): array
    {
        // possible query condition and page
//        $query = "
//        SELECT  `student`.`student_idx`
//             , `student_type`
//             , `student_name`
//             , `branch_idx`
//             , `student`.`created_at` as student_created_at
//            , mh.end_at
//            , mh.created_at as mh_created_at
//            , mh.started_at
//             , mh.ended_at
//             , time_to_sec( mh.ended_at)-  time_to_sec( mh.started_at) as 'ccc'
//             , case when mh.end_at is null then
//                   case mh.resource_idx when 0 then
//                       case when time_to_sec(now()) - time_to_sec(mh.started_at) > (120-3) then mh.ended_at
//                       else  NULL  END -- in progress
//                   else
//                       case when time_to_sec(now()) - time_to_sec(mh.started_at) > (60*15 -3 )then mh.ended_at
//                       else NULL
//                           END -- in progress
//                       END
//                ELSE mh.end_at END AS 'finaL_end_at'
//       , mh.meditation_history_idx
//        FROM `mave33`.student AS `student`
//        LEFT JOIN (
//            SELECT mh.meditation_history_idx
//            , mh.student_idx
//            , mh.start_at
//            , mh.created_at
//            , mh.end_at
//                 ,mh.resource_idx
//           , min(mhl.created_at)  AS 'started_at'
//            , max(mhl.created_at) AS 'ended_at'
//            FROM `mave33`.meditation_history mh
//            INNER JOIN (
//                SELECT student_idx,
//                       MAX(meditation_history_idx) meditation_history_idx
//                FROM `mave33`.meditation_history
//                GROUP BY student_idx
//            ) mh_inner
//            on mh.meditation_history_idx = mh_inner.meditation_history_idx
//            left JOIN mave33.meditation_history_log mhl
//            ON mh.meditation_history_idx = mhl.meditation_history_idx
//            where mh.resource_idx is not null
//            group by mh.meditation_history_idx, mh.student_idx -- , mh.start_at, mh.created_at, mh.end_at
//
//            ) AS mh
//       ON mh.student_idx = `student`.student_idx
//        WHERE branch_idx = ?;
//        ";

        $query = "
         SELECT st.student_idx
         , st.student_name
         , st.student_type
          , st.created_at as 'student_created_at'
         , mh_agg.created_at
              , mh_agg.created_at as mh_created_at
         , mh_agg.end_at
         , mh_agg.meditation_history_idx
         , mh_agg.start_record_time
         , mh_agg.end_record_time
         , TO_SECONDS(NOW()) - TO_SECONDS(mh_agg.end_record_time)  as time_diff
         , TO_SECONDS(NOW()) - TO_SECONDS(mh_agg.start_record_time)  as start_time_diff
               , case when mh_agg.end_at is null then
                   case mh_agg.resource_idx when 0 then
                       case when TO_SECONDS(now()) - TO_SECONDS(mh_agg.start_record_time) > (120-3)
                           then mh_agg.end_record_time
                       else  NULL  END -- in progress
                   else
                       case when TO_SECONDS(now()) - TO_SECONDS(mh_agg.start_record_time) > (60*15 -3 )
                           then mh_agg.end_record_time
                       else NULL
                           END -- in progress
                       END
                ELSE mh_agg.end_at END AS 'finaL_end_at'
             
         , CASE WHEN mh_agg.created_at IS NULL THEN 'NONE' 
                WHEN TO_SECONDS(NOW()) - TO_SECONDS(mh_agg.end_record_time) < 10 THEN 'PROG'    
                ELSE 'DONE' END as 'mh_status' 
           FROM mave33.student st 
           LEFT JOIN  (
               SELECT mh.student_idx
                    , mh.meditation_history_idx
                    , mhl.start_record_time
                    , mhl.end_record_time 
                    , mh.created_at
                    , mh.end_at
                    , mh.resource_idx
                 FROM mave33.meditation_history mh 
                 LEFT JOIN ( 
                     SELECT mhl.meditation_history_idx
                     , MIN(mhl.created_at) as `start_record_time`
                     , MAX(mhl.created_at) as `end_record_time`
                     FROM mave33.meditation_history_log mhl
                     GROUP BY  mhl.meditation_history_idx
                 ) mhl
                 ON mh.meditation_history_idx = mhl.meditation_history_idx
                 INNER JOIN ( 
                     SELECT student_idx 
                          , MAX(meditation_history_idx)  as meditation_history_idx 
                     FROM mave33.meditation_history 
                     GROUP BY student_idx
                ) mh_max
               ON mh_max.student_idx = mh.student_idx AND mh_max.meditation_history_idx = mh.meditation_history_idx
           ) mh_agg
          ON mh_agg.student_idx = st.student_idx
          WHERE branch_idx = ? 
        ";

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(1, $branch_idx, \PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetchAll();

        return $result;
    }

    public function getName($idx): array
    {
        $query = "
        SELECT  branch_name
         FROM `mave33`.`branch`
        WHERE 1=1 AND branch_idx = ?
        LIMIT 1;
        ";
        $stmt = $this->database->prepare($query);
        $stmt->bindParam(1, $idx, \PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetch();

        return $result;

    }

    public function getStudentInfo($student_idx)
    {
        $query = "
        SELECT s.student_idx, b.branch_idx, b.branch_name, s.student_name, s.student_type, s.created_at 
         FROM mave33.student s 
        INNER JOIN mave33.branch b
        ON s.branch_idx = b.branch_idx
        WHERE s.student_idx =? 
        ";
        $stmt = $this->database->prepare($query);
        $stmt->bindParam(1, $student_idx, \PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetch();

        return $result;
    }

    /**
     *  If inserted successfuly  return last inserted id or 0;
     * @param $branch_idx
     * @param $student_type
     * @param $student_name
     * @return int
     */
    public function create($branch_idx, $student_type, $student_name, $phone_number): int
    {

        $query = "
        INSERT INTO mave33.student  
          SET branch_idx = :branch_idx
            , student_type = :student_type
            , student_name = :student_name
            , phone_number = :phone_number
            , created_at= NOW() ; 
        ";

        $row = ["branch_idx" => $branch_idx
            , "student_type" => $student_type
            , "student_name" => $student_name
            , "phone_number" => $phone_number
        ];
        $stmt = $this->connection->prepare($query);

        if (!$stmt) {
            $this->error_message = $this->connection->errorInfo();
        }

        if ($stmt && $stmt->execute($row)) {
            return (int)$this->connection->lastInsertId();
        } else {
            $this->logger->debug("ERRROR : " . $this->connection->errorInfo());
            $this->error_message = $this->connection->errorInfo();
            return 0;
        }

    }
}