<?php

declare(strict_types=1);

namespace App\Service;

// Exception needed to be created

use App\Repository\AdminRepository;
use Monolog\Logger;

class AdminService extends BaseService
{
    /**
     * @var MeditationHistoryLogRepository
     */
    protected $repository;
    protected $logger;

    public function __construct(AdminRepository $repository, Logger $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    public function getAdminInfo(String $admin_id, String $pass){
        $admin = $this->repository->getAdminInfo($admin_id);
        // TODO :  Authentification logic requried
        $this->logger->debug("getAdminInfo: ".json_encode($admin));
        return $admin;
    }

    public function updateComport ( int $admin_idx, int $comport)
    {
        $rowcount = $this->repository->updateComport($admin_idx, $comport);
        return $rowcount;
    }

}