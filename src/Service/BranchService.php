<?php
declare(strict_types=1);

namespace App\Service;


use App\Repository\BranchRepository;

class BranchService extends BaseService
{

    protected $repository;

    public function __construct(BranchRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        return $this->repository->getAll();
    }

    public function getBranchName($idx)
    {
        return $this->repository->getName($idx);
    }

}