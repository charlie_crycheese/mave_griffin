<?php

declare(strict_types=1);

namespace App\Service;

// Exception needed to be created

use App\Repository\MeditationHistoryLogRepository;
use Monolog\Logger;

class MeditationHistoryLogService extends BaseService
{
    /**
     * @var MeditationHistoryLogRepository
     */
    protected $meditationHistoryLogRepository;

    public function __construct(MeditationHistoryLogRepository $meditationHistoryLogRepository, Logger $logger)
    {
        $this->meditationHistoryLogRepository = $meditationHistoryLogRepository;
        $this->logger = $logger;
    }

    public function getTen(int $idx, int $page): array
    {
        return $this->meditationHistoryLogRepository->getTenResult($idx, $page);
    }

    /**
     *  start_time : YYYYMMDDHHMMSS
     * @param int $idx
     * @param $startTime
     * @param int $page
     * @return array
     */
    public function getTenRecent(int $idx, $startTime, int $page): array
    {

        $start_time = $this->convert2datetime($startTime);

        return $this->meditationHistoryLogRepository->getTenRecent($idx, $startTime, $page);
    }

    /**
     * take mhIdx and return the initial data from the current time - 30 seconds data
     * @param int $mhIdx
     * @return array
     */
    public function getInitialResultForLiveStream(int $mhIdx): array
    {
        return $this->meditationHistoryLogRepository->getInitialResultForLiveStream($mhIdx);
    }

    protected function convert2datetime($time)
    {
        // check 12 digits  (YYYYMMDDHHMMSS) => YYYY-MM-DD HH:MM:SS )
        if (count($time) === 14) {
            $yyyy = substr($time, 0, 4);
            $mm = substr($time, 4, 2);
            $dd = substr($time, 6, 2);
            $hh = substr($time, 8, 2);
            $mm = substr($time, 10, 2);
            $ss = substr($time, 12, 2);

            $return_str = "{$yyyy}-{$mm}-{$dd} {$hh}:{$mm}:{$ss}";
            return $return_str;
        } else {
            return "";
        }
    }


}