<?php

declare(strict_types=1);

namespace App\Service;

// Exception needed to be created

use App\Repository\MeditationHistoryRepository;
use Monolog\Logger;

class MeditationHistoryService extends BaseService
{
    /**
     * @var MeditationHistoryRepository
     */
    protected $meditationHistoryRepository;

    public function __construct(MeditationHistoryRepository $meditationHistoryRepository, Logger $logger)
    {
        $this->meditationHistoryRepository = $meditationHistoryRepository;
        $this->logger = $logger;
    }

    public function createInit( int $student_idx){
        return $this->meditationHistoryRepository->create($student_idx);
    }

    public function createStudentResource(int $student_idx, int $resource_idx){
        return $this->meditationHistoryRepository->createWithStudentResource($student_idx, $resource_idx);
    }

    public function getMeditationHistoryWithStudent( $student_idx ){
        return $this->meditationHistoryRepository->getMostRecentHistoryIdx($student_idx);
    }
    public function  getMeditationHistoryForReport ( $mh_idx ){
        return $this->meditationHistoryRepository->getMeditationHistoryForReport($mh_idx);
    }

    public function getAggreagtedMeditationHistoryLogForReport ($mh_idx){
        $result = $this->meditationHistoryRepository->getAggreagtedMeditationHistoryLogForReport($mh_idx);
//       $this->logger->debug(json_encode($result));
       return $result;
    }

    public function  getMeditationHistoryLogsForReport ($mh_idx){
        $result = $this->meditationHistoryRepository->getMeditationHistoryLogsForReport($mh_idx);
//       $this->logger->debug(json_encode($result));
        return $result;
    }

    public function updateMhData( $mh_idx, $mhData){
        $this->logger->debug("*****************UPDATE MH DATA ".PHP_EOL.json_encode($mhData));
        $result = $this->meditationHistoryRepository->updateMh($mh_idx, $mhData);
        return $result;
    }

    public function updateEndAt( $meditation_history_idx){
        return $this->meditationHistoryRepository->updateDateColumn($meditation_history_idx,"end_at");
    }
    public function updateStartAt( $meditation_history_idx){
        return $this->meditationHistoryRepository->updateDateColumn($meditation_history_idx,"start_at");
    }
    public function getListByStudent($student_idx){
        return $this->meditationHistoryRepository->getMeditationHistoryByStudent($student_idx);
    }

    public function getBaseLineMhIdxByStudentIdx($student_idx){
        return $this->meditationHistoryRepository->getMostRecentBaseLineMhIdxByStudentIdx($student_idx);
    }

}