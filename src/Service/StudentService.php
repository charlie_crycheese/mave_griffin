<?php

declare(strict_types=1);

namespace App\Service;

// Exception needed to be created

use App\Repository\StudentRepository;
use Monolog\Logger;

class StudentService extends BaseService
{
    /**
     * @var MeditationHistoryLogRepository
     */
    protected $repository;
    protected $logger;

    public function __construct(StudentRepository $repository, Logger $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    public function getAll(int $idx):array{
        return $this->repository->getAll($idx);
    }

    /**
     * @param $branch_idx
     * @param $student_type
     * @param $student_name
     * @return int inserted id
     */
    public function create( $branch_idx, $student_type, $student_name, $phone_number) {
        return $this->repository->create($branch_idx, $student_type, $student_name, $phone_number);
    }

}