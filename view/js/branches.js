(function ($) {
    "use strict"; // Start of use strict
    console.log('branches loaded');

    const remote_location = 'https://mave33.cafe24.com/mave_griffin/branches/';


    const fetchData = function (url, nextPage) {
        let requestUrl = remote_location + url;
       return   fetch(requestUrl)
            .then(res => {
                    return res.json();
                }
            );
    }

    const renderBranches = function (branches) {
        if (branches && branches.length > 0) {
            branches.map(branch => {
                let frag = `
                      <div class="col-xl-3 col-md-6 mb-4 pl-0">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">지점 등급</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">${branch.branch_name}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-${branch.icon} fa-2x text-primary"></i>
                                    </div>
                                </div>
                                <a href="branches/${branch.branch_idx}/students" class="stretched-link"></a>
                            </div>
                        </div>
                    </div>
                    `;

                $('.branch-row').append(frag);

            });


        } else {

        }
    }

    $(document).ready(function () {
        fetchData('All').then(res => {
            if (res && res.status === 'success') {
                $(".branch-row").empty();
                renderBranches(res.message);
            } else {
                console.log(' fail ');
            }
        });

    });

})(jQuery); // End of use strict
