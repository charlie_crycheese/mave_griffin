(function ($) {
    "use strict"; // Start of use strict
    const remote_location = 'https://mave33.cafe24.com/mave_griffin';
    const studentId = $("#student_id").val();
    console.log("studentId:" + studentId);
    var page = 0;
    const student_idx = $("#student_idx").val();
    const mh_idx = $("#mh_idx").val();
    const time = moment().subtract(3, 'minutes').format('YYYYMMDDHHmmss');
    // const time = moment().subtract(40,'seconds').format('YYYYMMDDHHmmss');
    // let requestLocation = remote_location + "/mh_log//page/";
    let requestLocation = `${remote_location}/mh_log/${mh_idx}/time/${time}/page/`;

    var config = {
        responsive: true
        , displayModeBar: false
        // , modeBarButtonsToRemove:['zoom2d','select2d','lasso2d','zoomIn2d','zoomOut2d','resetScale2d'
        //     ,'toggleSpikelines'
        //     , 'resetViewMapbox'
        //     ,'toImage'
        //     , 'hoverClosestGl2d'
        //     , 'hoverClosestPie'
        //     ,'plotlyLogoMark']
    };
    var layout = {
        font: {size: 12},
        height: 300,
        xaxis: {
            showgrid: true,
            zeroline: true,
            rangemode: "nonnegative",
        },
        yaxis: {
            zeroline: true

        },
        margin: {
            l: 40,
            r: 40,
            b: 50,
            t: 10,
            pad: 10
        }
    }


    var doubleTraceLayout = {
        font: {size: 8},
        height: 300,
        xaxis: {
            showgrid: true,
            zeroline: true,
            rangemode: "nonnegative",
        },
        yaxis: {
            zeroline: true

        },
        margin: {
            l: 20,
            r: 20,
            b: 20,
            t: 10,
            pad: 10
        },
        showLegend: true,
        legend:{'orientation':'h'}
    }

    var topLayout = {
        font: {size: 12},
        height: 300,
        xaxis: {
            showgrid: true,
            zeroline: true,
            rangemode: "nonnegative",
        },
        yaxis: {
            rangemode: 'tozero',
            range: [0, 100],
            autorange: false,
        },
        margin: {
            l: 40,
            r: 40,
            b: 50,
            t: 10,
            pad: 10
        }
    }


    var top2Layout = {
        font: {size: 12},
        height: 300,
        xaxis: {
            showgrid: true,
            zeroline: true,
            rangemode: "nonnegative",
        },
        margin: {
            l: 40,
            r: 20,
            b: 50,
            t: 10,
            pad: 10
        }
    }
    // TODO : need to have first count of how many avaialble

    /*
$app->get('/mh_log/{id}/time/{time}/page/{page:[0-9]+}',\App\Action\MediationHistoryLog\GetTenRecent::class);
     */
    $(document).ready(function () {
        // getInitialData();

        Plotly.plot('attentionChart', [{
            y: [],
            type: 'line'
        }], top2Layout, config);

       Plotly.plot('deltaChart',
            [
                { x:[], y:[], mode:'lines',color:'red',size:8, name:'Fp1' } ,
                { x:[], y:[], mode:'lines',color:'blue',size:8 ,name:'Fp2'}
            ],
            doubleTraceLayout, config);

        Plotly.plot('thetaChart',
            [
                {x:[],y:[], mode:'lines',color:'red',size:8, name:'Fp1' } ,
                {x:[],y:[], mode:'lines',color:'blue',size:8 ,name:'Fp2'}
            ],
            doubleTraceLayout, config);

        Plotly.plot('betaChart',
            [
                {x:[],y:[], mode:'lines',color:'red',size:8, name:'Fp1' } ,
                {x:[],y:[], mode:'lines',color:'blue',size:8 ,name:'Fp2'}
            ],
            doubleTraceLayout, config);

        Plotly.plot('alphaChart',
            [
                {x:[],y:[], mode:'lines',color:'red',size:8, name:'Fp1' } ,
                {x:[],y:[], mode:'lines',color:'blue',size:8 ,name:'Fp2'}
            ],
            doubleTraceLayout, config);

        Plotly.react('gammaChart', [
                {x:[],y:[], mode:'lines',color:'red',size:8, name:'Fp1' } ,
                {x:[],y:[], mode:'lines',color:'blue',size:8 ,name:'Fp2'}
        ], doubleTraceLayout, config);



        Plotly.plot('stressChart', [{

            y: [],
            type: 'line'
        }], top2Layout, config);

        Plotly.plot('sdnnChart', [{
            x:[],
            y: [],
            type: 'line'
        }], top2Layout, config);

        Plotly.plot('rmssdChart', [{
            x:[],
            y: [],
            type: 'line'
        }], top2Layout, config);
        Plotly.plot('bpmChart', [{
            x:[],
            y: [],
            type: 'line'
        }], top2Layout, config);

        showChart(requestLocation, page);
        // fetchData(requestLocation,page);

    }); // end of document ready


    const delay = (ms) => {
        var start = new Date().getTime();
        var end = start;
        while (end < start + ms) {
            end = new Date().getTime();
        }
    };
    let first = true;

    var xValue = 0;
    async function getInitialData(){

        let initialLocation = `${remote_location}/mh_log/${mh_idx}`;
        let response = await fetch(initialLocation);
        console.log(response);
        // Plotly.plot('attentionChart', [{
        //     y: [],
        //     type: 'line'
        // }], top2Layout, config);

    }
    async function showChart(url, nextPage) {
        let requestUrl = url + nextPage;
        let resposne = await fetch(requestUrl);
        let data = await resposne.json();
        data = data.message.result;
        // let fp1Deltas = await data.map(e=>e.fp1_delta);
        // let streeses = await data.map(e=>e.stress);
        // let bpms = await data.map(e=>e.bpm);
        // console.log(fp1Deltas);




        // let nIntervalId = setInterval(drawAll, 1000);
        for (const datum of data) {
            if (first) {
                first = false;
                $("#currentStartTime").text(datum.created_at);
            } else {
                // console.log(new Date().getTime());
                await sleep(1000);
            }
            $("#currentLastTime").text(datum.created_at);
            let attention = datum.attention;
            let stress = datum.stress;
            let sdnn = datum.sdnn;
            let rmssd = datum.rmssd;
            let bpm = datum.bpm;
            xValue= xValue +1;
            drawChart('attentionChart', attention);
            draw2traceChart('gammaChart', datum.fp1_gamma, datum.fp2_gamma,xValue);
            draw2traceChart('alphaChart', datum.fp1_alpha, datum.fp2_alpha,xValue);
            draw2traceChart('thetaChart', datum.fp1_theta, datum.fpt2_theta, xValue);
            draw2traceChart('betaChart', datum.fp1_beta, datum.fp2_beta, xValue);
            draw2traceChart('deltaChart', datum.fp1_delta, datum.fp2_delta,xValue);
            drawChart('stressChart', stress);
            drawChartWithLimit('sdnnChart', sdnn,xValue);
            drawChartWithLimit('rmssdChart', rmssd, xValue);
            drawChartWithLimit('bpmChart', bpm,xValue);
        }


        if (data.length > 9) {
            console.log('done');
            showChart(url, nextPage + 1);
        } else {
            console.log('DONE');
        }
    }

    const sleep = ms => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    //
    const drawChart = (chartType, point) => {

        Plotly.extendTraces(chartType, {
            y: [
                [point]
            ],
        }, [0]);
    }

    const drawChartWithLimit = ( chartType, point , xValue)=>{
        Plotly.extendTraces(chartType, {
            x:[[xValue]],
            y: [
                [point]
            ],
        }, [0], 60);
    }


    const draw2traceChart = (chartType, point1, point2, xValue) => {

        Plotly.extendTraces(chartType, {
            x:[[xValue],[xValue]],
            y: [
                [point1], [point2]
            ]
        }, [0, 1], 60);
    }


    function fetchData(url, nextPage) {
        let requestUrl = url + nextPage;
        fetch(requestUrl)
            .then(res => res.json())
            .then(data => {
                // console.log(data.message.result);
                let length = data.message.result.length;
                let fp1_deltas = data.message.result.map(e => e.fp1_delta);
                let count = 0;
                var isLoopDone = false;

                return fp1_deltas.length;
            }).then(isReturn => {
            console.log(isReturn);
            // if(  length  > 9 ){
            //     page++;
            //     // delay(10000);
            //     fetchData(url, page);
            // }
        });
    }


    /**
     Plotly.extendTraces('chart', {
                        y: [
                            [pointData[1]]
                        ]
                    }, [0]);
     */
})
(jQuery); // End of use strict
