(function ($,regression, jStat) {
    "use strict"; // Start of use strict
    const remote_location = 'https://mave33.cafe24.com/mave_griffin';
    const studentId = $("#student_id").val();
    console.log("studentId:" + studentId);
    var page = 0;
    const student_idx = $("#student_idx").val();
    // const time = moment().subtract(40,'seconds').format('YYYYMMDDHHmmss');
    // let requestLocation = remote_location + "/mh_log//page/";
    // let requestLocation = `${remote_location}/mh_log/${mh_idx}/time/${time}/page/`;

    const config = {
        responsive: true
        , displayModeBar: false
        // , modeBarButtonsToRemove:['zoom2d','select2d','lasso2d','zoomIn2d','zoomOut2d','resetScale2d'
        //     ,'toggleSpikelines'
        //     , 'resetViewMapbox'
        //     ,'toImage'
        //     , 'hoverClosestGl2d'
        //     , 'hoverClosestPie'
        //     ,'plotlyLogoMark']
    };
    const pieLayout = {
        font: {size: 6},
        height: 250,
        width: 320,
        margin: {
            l: 0,
            r: 0,
            b: 0,
            t: 0,
            pad: 0
        },
        legend: {
            valign: "bottom",
            orientation: "h",
        }
    }

    const fullWidthLayout = {
        font: {size: 10},
        width: 660,
        height: 400,
        margin: {
            l: 50,
            r: 10,
            b: 10,
            t: 10,
            pad: 0
        },
        barmode: 'group',
        legend: {
            valign: "bottom",
            orientation: "h",
            x: 0.4,
            xanchor: 'center'
        }
    }

    const doubleChartWidthLayout = {
        font: {size: 10},
        width: 660,
        height: 220,
        margin: {
            l: 50,
            r: 10,
            b: 10,
            t: 10,
            pad: 10
        },
        barmode: 'group',
        legend: {
            valign: "bottom",
            orientation: "h",
            x: 0.4,
            y: -0.5,
            yanchor: 'bottom',
            xanchor: 'center'
        }
    }


    const tripleChartWidthLayout = {
        font: {size: 10},
        width: 660,
        height: 155,
        margin: {
            l: 50,
            r: 10,
            b: 30,
            t: 10,
            pad:10
        },
        legend: {
            valign: "bottom",
            orientation: "h",
            x: 0.4,
            y: -0.5,
            yanchor: 'bottom',
            xanchor: 'center'
        }
    }


    /*
$app->get('/mh_log/{id}/time/{time}/page/{page:[0-9]+}',\App\Action\MediationHistoryLog\GetTenRecent::class);
     */

    async function showAllChart() {
        let aggMhr = await new Promise((resolve) => {
            let values = document.querySelector('#aggMhr').dataset.aggmhr
            resolve(
                JSON.parse(values)
            );
        });
        let allMhrLog = await new Promise((resolve) => {
            let values = document.querySelector('#allMhrLog').dataset.allmhrlog
            resolve(
                JSON.parse(values)
            );
        });
        let mhr = await new Promise((resolve) => {
            let values = document.querySelector('#mhr').dataset.mhr;
            resolve(
                JSON.parse(values)
            );
        });
        let baselineData = await new Promise((resolve) => {
            let values = document.querySelector('#baselineData').dataset.data;
            resolve(
                JSON.parse(values)
            );
        });

        // console.log("mhr::: " );
        // console.log(mhr);
        console.log('abseline');
        console.log(baselineData);

        aggMhr = aggMhr[0];
        let frequencyResultPieChartData =
            await new Promise(resolve => {
                let frequencyResultPieChartData =
                    [{
                        values: [aggMhr.avg_delta, aggMhr.avg_theta, aggMhr.avg_alpha, aggMhr.avg_beta, aggMhr.avg_gamma],
                        labels: ['Delta', 'Theta', 'Alpha', 'Beta', 'Gamma'],
                        type: 'pie',
                    }];
                Plotly.newPlot('frequencyResultPieChart', frequencyResultPieChartData, pieLayout, config);
                const xAxis15min = ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'];

                    let delta = {
                    x: xAxis15min, // ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.delta_avg),
                    name: 'delta',
                    type: 'bar'
                }

                let theta = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.theta_avg),
                    name: 'theta',
                    type: 'bar'
                }
                let alpha = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.alpha_avg),
                    name: 'alpha',
                    type: 'bar'
                };

                let beta = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.beta_avg),
                    name: 'beta',
                    type: 'bar'
                }

                let gamma = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.gamma_avg),
                    name: 'gamma',
                    type: 'bar'
                }
                let totalFrequencyAvgResultChartData = [delta, theta, alpha, beta, gamma];

                Plotly.newPlot('totalFrequencyAvgResultChart', totalFrequencyAvgResultChartData, fullWidthLayout, config);


                let delta_fp1 = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.fp1_delta_avg),
                    name: 'Fp1',
                    type: 'bar'
                }

                let delta_fp2 = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.fp2_delta_avg),
                    name: 'Fp2',
                    type: 'bar'
                }

                // console.log(deltaMeanValues);
                let deltaAvg = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.delta_avg),
                    name: 'Mean',
                    type: 'line'
                }

                let maxFp1Delta = allMhrLog.reduce((acc, cur) => Number(acc.fp1_delta_avg) > Number(cur.fp2_delta_avg) ? acc : cur);
                let maxFp2Delta = allMhrLog.reduce((acc, cur) => Number(acc.fp2_delta_avg) > Number(cur.fp2_delta_avg) ? acc : cur);
                let minFp1Delta = allMhrLog.reduce((acc, cur) => Number(acc.fp1_delta_avg) > Number(cur.fp1_delta_avg) ? cur : acc);
                let minFp2Delta = allMhrLog.reduce((acc, cur) => Number(acc.fp2_delta_avg) > Number(cur.fp2_delta_avg) ? cur : acc);

                document.querySelector("#fp1DeltaMaxTime").textContent = maxFp1Delta.minute;
                document.querySelector("#fp2DeltaMaxTime").textContent = maxFp2Delta.minute;
                document.querySelector("#fp1DeltaMinTime").textContent = minFp1Delta.minute;
                document.querySelector("#fp2DeltaMinTime").textContent = minFp2Delta.minute;

                let deltaPowerChartData = [delta_fp1, delta_fp2, deltaAvg];
                Plotly.newPlot('deltaPowerChart', deltaPowerChartData, doubleChartWidthLayout, config);

                let theta_fp1 = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.fp1_theta_avg),
                    name: 'Fp1',
                    type: 'bar'
                }

                let theta_fp2 = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.fp2_theta_avg),
                    name: 'Fp2',
                    type: 'bar'
                }

                // console.log(thetaMeanValues);
                let thetaAvg = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.theta_avg),
                    name: 'Mean',
                    type: 'line'
                }

                let maxFp1Theta = allMhrLog.reduce((acc, cur) => Number(acc.fp1_theta_avg) > Number(cur.fp1_theta_avg) ? acc : cur);
                let maxFp2Theta = allMhrLog.reduce((acc, cur) => Number(acc.fp2_theta_avg) > Number(cur.fp2_theta_avg) ? acc : cur);
                let minFp1Theta = allMhrLog.reduce((acc, cur) => Number(acc.fp1_theta_avg) > Number(cur.fp1_theta_avg) ? cur : acc);
                let minFp2Theta = allMhrLog.reduce((acc, cur) => Number(acc.fp2_theta_avg) > Number(cur.fp2_theta_avg) ? cur : acc);

                document.querySelector("#fp1ThetaMaxTime").textContent = maxFp1Theta.minute;
                document.querySelector("#fp2ThetaMaxTime").textContent = maxFp2Theta.minute;
                document.querySelector("#fp1ThetaMinTime").textContent = minFp1Theta.minute;
                document.querySelector("#fp2ThetaMinTime").textContent = minFp2Theta.minute;

                let thetaPowerChartData = [theta_fp1, theta_fp2, thetaAvg];
                Plotly.newPlot('thetaPowerChart', thetaPowerChartData, doubleChartWidthLayout, config);


                let alpha_fp1 = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.fp1_alpha_avg),
                    name: 'Fp1',
                    type: 'bar'
                }

                let alpha_fp2 = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.fp2_alpha_avg),
                    name: 'Fp2',
                    type: 'bar'
                }

                // console.log(alphaMeanValues);
                let alphaAvg = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.alpha_avg),
                    name: 'Mean',
                    type: 'line'
                }

                let maxFp1Alpha = allMhrLog.reduce((acc, cur) => Number(acc.fp1_alpha_avg) > Number(cur.fp1_alpha_avg) ? acc : cur);
                let maxFp2Alpha = allMhrLog.reduce((acc, cur) => Number(acc.fp2_alpha_avg) > Number(cur.fp2_alpha_avg) ? acc : cur);
                let minFp1Alpha = allMhrLog.reduce((acc, cur) => Number(acc.fp1_alpha_avg) > Number(cur.fp1_alpha_avg) ? cur : acc);
                let minFp2Alpha = allMhrLog.reduce((acc, cur) => Number(acc.fp2_alpha_avg) > Number(cur.fp2_alpha_avg) ? cur : acc);

                document.querySelector("#fp1AlphaMaxTime").textContent = maxFp1Alpha.minute;
                document.querySelector("#fp2AlphaMaxTime").textContent = maxFp2Alpha.minute;
                document.querySelector("#fp1AlphaMinTime").textContent = minFp1Alpha.minute;
                document.querySelector("#fp2AlphaMinTime").textContent = minFp2Alpha.minute;

                let alphaPowerChartData = [alpha_fp1, alpha_fp2, alphaAvg];
                Plotly.newPlot('alphaPowerChart', alphaPowerChartData, doubleChartWidthLayout, config);


                let beta_fp1 = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.fp1_beta_avg),
                    name: 'Fp1',
                    type: 'bar'
                }

                let beta_fp2 = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.fp2_beta_avg),
                    name: 'Fp2',
                    type: 'bar'
                }

                // console.log(betaMeanValues);
                let betaAvg = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.beta_avg),
                    name: 'Mean',
                    type: 'line'
                }

                let maxFp1Beta = allMhrLog.reduce((acc, cur) => Number(acc.fp1_beta_avg) > Number(cur.fp1_beta_avg) ? acc : cur);
                let maxFp2Beta = allMhrLog.reduce((acc, cur) => Number(acc.fp2_beta_avg) > Number(cur.fp2_beta_avg) ? acc : cur);
                let minFp1Beta = allMhrLog.reduce((acc, cur) => Number(acc.fp1_beta_avg) > Number(cur.fp1_beta_avg) ? cur : acc);
                let minFp2Beta = allMhrLog.reduce((acc, cur) => Number(acc.fp2_beta_avg) > Number(cur.fp2_beta_avg) ? cur : acc);

                document.querySelector("#fp1BetaMaxTime").textContent = maxFp1Beta.minute;
                document.querySelector("#fp2BetaMaxTime").textContent = maxFp2Beta.minute;
                document.querySelector("#fp1BetaMinTime").textContent = minFp1Beta.minute;
                document.querySelector("#fp2BetaMinTime").textContent = minFp2Beta.minute;

                let betaPowerChartData = [beta_fp1, beta_fp2, betaAvg];
                Plotly.newPlot('betaPowerChart', betaPowerChartData, doubleChartWidthLayout, config);


                let gamma_fp1 = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.fp1_gamma_avg),
                    name: 'Fp1',
                    type: 'bar'
                }

                let gamma_fp2 = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.fp2_gamma_avg),
                    name: 'Fp2',
                    type: 'bar'
                }

                // console.log(gammaMeanValues);
                let gammaAvg = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e => e.gamma_avg),
                    name: 'Mean',
                    type: 'line'
                }

                let maxFp1Gamma = allMhrLog.reduce((acc, cur) => Number(acc.fp1_gamma_avg) > Number(cur.fp1_gamma_avg) ? acc : cur);
                let maxFp2Gamma = allMhrLog.reduce((acc, cur) => Number(acc.fp2_gamma_avg) > Number(cur.fp2_gamma_avg) ? acc : cur);
                let minFp1Gamma = allMhrLog.reduce((acc, cur) => Number(acc.fp1_gamma_avg) > Number(cur.fp1_gamma_avg) ? cur : acc);
                let minFp2Gamma = allMhrLog.reduce((acc, cur) => Number(acc.fp2_gamma_avg) > Number(cur.fp2_gamma_avg) ? cur : acc);

                document.querySelector("#fp1GammaMaxTime").textContent = maxFp1Gamma.minute;
                document.querySelector("#fp2GammaMaxTime").textContent = maxFp2Gamma.minute;
                document.querySelector("#fp1GammaMinTime").textContent = minFp1Gamma.minute;
                document.querySelector("#fp2GammaMinTime").textContent = minFp2Gamma.minute;

                let gammaPowerChartData = [gamma_fp1, gamma_fp2, gammaAvg];
                Plotly.newPlot('gammaPowerChart', gammaPowerChartData, doubleChartWidthLayout, config);

                // attention
                // console.log(gammaMeanValues);
                const attentionAvgData = allMhrLog.map(e=>Number(e.attention_avg));
                let attentionAvg = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: attentionAvgData , // allMhrLog.map(e =>Number( e.attention_avg)),
                    name: 'Mean',
                    type: 'line'
                }

                let maxAttention = allMhrLog.reduce((acc, cur) => Number(acc.attention_avg) > Number(cur.attention_avg) ? acc : cur);
                let minAttention = allMhrLog.reduce((acc, cur) => Number(acc.attention_avg) > Number(cur.attention_avg) ? cur : acc);
                let sumAttention = {
                    sumx: 0, sumy: 0, sumxx: 0, sumyy: 0, sumxy: 0
                }

                allMhrLog.map((e, index) => {
                        sumAttention.sumx += (index + 1);
                        sumAttention.sumy += Number(e.attention_avg);
                        sumAttention.sumxx += (index + 1) * (index + 1);
                        sumAttention.sumyy += Number(e.attention_avg) * Number(e.attention_avg);
                        sumAttention.sumxy += (index + 1) * Number(e.attention_avg);
                    }
                );
                let attentionLength = allMhrLog.length;

                let slope = (attentionLength * sumAttention.sumxy - sumAttention.sumx* sumAttention.sumy )/ ( attentionLength * sumAttention.sumxx - Math.pow(sumAttention.sumx,2));
                let intercept = ( sumAttention.sumy - slope * sumAttention.sumx)/attentionLength;
                let linearRegressionData = allMhrLog.map( ( e,index)=>slope*(index+1)+intercept);
                let coefficient =(attentionLength*sumAttention.sumxy - sumAttention.sumx* sumAttention.sumy)/Math.sqrt((attentionLength* sumAttention.sumxx-sumAttention.sumx* sumAttention.sumx)*(attentionLength*sumAttention.sumyy-sumAttention.sumy*sumAttention.sumy))
                // const result = regression;
                console.log("COFFEFICNT %d", coefficient);

                let linearRegressionTrace = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: linearRegressionData,
                    name: 'regression',
                    mode: 'lines',
                    line:{
                       dash:'dashdot',
                       width:2,
                    }
                }

                document.querySelector("#attentionMaxTime").textContent = maxAttention.minute;
                document.querySelector("#attentionMinTime").textContent = minAttention.minute;
                document.querySelector("#attentionCorrelation").textContent = (coefficient  <0?'Negative':'Positive')
                    +"("+ coefficient.toFixed(2) +")";



                let attentionPowerChartData = [attentionAvg,linearRegressionTrace];
                Plotly.newPlot('attentionPowerChart', attentionPowerChartData, doubleChartWidthLayout, config);

               // Heart rate
                // console.log(gammaMeanValues);
                let bpmAvg = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e =>Number( e.bpm_avg)),
                    name: 'Mean',
                    type: 'line'
                }

                let maxBpm = allMhrLog.reduce((acc, cur) => Number(acc.bpm_avg) > Number(cur.bpm_avg) ? acc : cur);
                let minBpm = allMhrLog.reduce((acc, cur) => Number(acc.bpm_avg) > Number(cur.bpm_avg) ? cur : acc);

                document.querySelector("#bpmMaxTime").textContent = maxBpm.minute;
                document.querySelector("#bpmMinTime").textContent = minBpm.minute;


                let bpmPowerChartData = [bpmAvg];
                Plotly.newPlot('bpmPowerChart', bpmPowerChartData, tripleChartWidthLayout, config);

                // sdnn
                let sdnnAvg = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e =>Number( e.sdnn_avg)),
                    name: 'Mean',
                    type: 'line'
                }

                let maxSdnn = allMhrLog.reduce((acc, cur) => Number(acc.sdnn_avg) > Number(cur.sdnn_avg) ? acc : cur);
                let minSdnn = allMhrLog.reduce((acc, cur) => Number(acc.sdnn_avg) > Number(cur.sdnn_avg) ? cur : acc);

                document.querySelector("#sdnnMaxTime").textContent = maxSdnn.minute;
                document.querySelector("#sdnnMinTime").textContent = minSdnn.minute;


                let sdnnPowerChartData = [sdnnAvg];
                Plotly.newPlot('sdnnPowerChart', sdnnPowerChartData, tripleChartWidthLayout, config);

                // rmssd
                let rmssdAvg = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: allMhrLog.map(e =>Number( e.rmssd_avg)),
                    name: 'Mean',
                    type: 'line'
                }

                let maxRmssd = allMhrLog.reduce((acc, cur) => Number(acc.rmssd_avg) > Number(cur.rmssd_avg) ? acc : cur);
                let minRmssd = allMhrLog.reduce((acc, cur) => Number(acc.rmssd_avg) > Number(cur.rmssd_avg) ? cur : acc);

                document.querySelector("#rmssdMaxTime").textContent = maxRmssd.minute;
                document.querySelector("#rmssdMinTime").textContent = minRmssd.minute;


                let rmssdPowerChartData = [rmssdAvg];
                Plotly.newPlot('rmssdPowerChart', rmssdPowerChartData, tripleChartWidthLayout, config);


                let frequencyDomainResultPieChartData =
                    [{
                        values: [mhr.vlf_pct, mhr.lf_pct, mhr.hf_pct],
                        labels: ['VLF(%)', 'LF(%)','HF(%)'],
                        type: 'pie',
                    }];
                let domainPieLayout = pieLayout;
                domainPieLayout.font.size = 10;
                domainPieLayout.margin.l = 10;

                Plotly.newPlot('frequencyDomainResultPieChart', frequencyDomainResultPieChartData, pieLayout, config);


                // ans analysis
                const xAxis100 = [20, 40, 60, 80, 100];
                let sns  = mhr.ans_balance_pct;
                let pns =( 100- mhr.ans_balance_pct).toFixed(2) ;
                let snsTrace = {
                    x: [sns],
                    y:[''],
                    name:'SNS',
                    orientation : 'h',
                    marker:{
                        color:'rgba(55,128,191,0.6)',
                        width: 5,
                    },
                    // text:'aaaaaaa',
                    // textposition:'inside',
                    // mode:'markers+text',
                    // outsidetextfont:{size:10},
                    type:'bar',
                }
                let pnsTrace = {
                    x: [pns],
                    y:[''],
                    name:'PNS',
                    orientation : 'h',
                    marker:{
                        color:'rgba(255,153,51,0.6)',
                        width: 5,
                    },
                    type:'bar'
                }
                let ansBalanceChart = [snsTrace, pnsTrace];
                let horizontalBarLayout = {
                   title: {
                      text: 'Autonomic Balance Result',
                       y:0.95,
                       x:0.07,
                       yanchor:'top',
                   },
                    barmode: 'stack',
                    width: 660,
                    height: 180,
                    xaxis:{
                        showgrid: true,
                        gridcolor:'#000',
                        gridwidth: 1,
                    },
                    margin: {
                        l: 50,
                        r: 10,
                        b: 50,
                        t: 70,
                        pad: 10
                    },
                    legend: {
                        valign: "bottom",
                        orientation: "h",
                         x: 0.4,
                         y: -2,
                        yanchor: 'bottom',
                        xanchor: 'center',
                        traceorder:'normal'
                    },
                    annotations:[
                        {
                            text:`SNS Activity: ${sns}(%)` ,
                            align: 'left',
                            showarrow: false,
                            xref:'paper',
                            yref:'paper',
                            x: 0.1,
                            y:2,
                        },
                        {
                            text:`PNS Activity: ${pns} (%)`,
                            align: 'left',
                            showarrow: false,
                            xref:'paper',
                            yref:'paper',
                            x: 0.85,
                            y:2,
                        },
                    ]

                }

                Plotly.newPlot('ansBalanceChart', ansBalanceChart, horizontalBarLayout, config);

                // stress level
                const stressAvgData = allMhrLog.map(e=>Number(e.stress_avg));
                let avgBaseLineData;
                let baselineTrace = null ;
               if( baselineData.length > 0){
                    avgBaseLineData = 0;
                    baselineData.map( e=> { avgBaseLineData = (Number(e.avg_stress)+avgBaseLineData)});
                   avgBaseLineData = avgBaseLineData / baselineData.length;

                   console.log("avgBas" + avgBaseLineData);
                    let baselineData15 = Array(15).fill(avgBaseLineData);
                    baselineTrace ={
                        x:xAxis15min,
                        y:baselineData15,
                        name: '기준측정',
                        type:'line'
                    }
               }

                let stressAvg = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: stressAvgData , // allMhrLog.map(e =>Number( e.stress_avg)),
                    name: '스트레스',
                    type: 'line'
                }

                let maxStress = allMhrLog.reduce((acc, cur) => Number(acc.stress_avg) > Number(cur.stress_avg) ? acc : cur);
                let minStress = allMhrLog.reduce((acc, cur) => Number(acc.stress_avg) > Number(cur.stress_avg) ? cur : acc);
                let sumStress = {
                    sumx: 0, sumy: 0, sumxx: 0, sumyy: 0, sumxy: 0
                }

                allMhrLog.map((e, index) => {
                        sumStress.sumx += (index + 1);
                        sumStress.sumy += Number(e.stress_avg);
                        sumStress.sumxx += (index + 1) * (index + 1);
                        sumStress.sumyy += Number(e.stress_avg) * Number(e.stress_avg);
                        sumStress.sumxy += (index + 1) * Number(e.stress_avg);
                    }
                );
                let stressLength = allMhrLog.length;

                let stressSlope = (stressLength * sumStress.sumxy - sumStress.sumx* sumStress.sumy )/
                    ( stressLength * sumStress.sumxx - Math.pow(sumStress.sumx,2));
                let stressIntercept = ( sumStress.sumy - stressSlope * sumStress.sumx)/stressLength;

                let stressLinearRegressionData = allMhrLog.map( ( e,index)=>stressSlope *(index+1)+stressIntercept);

                let stressCoefficient =(stressLength*sumStress.sumxy - sumStress.sumx* sumStress.sumy)/Math.sqrt((stressLength* sumStress.sumxx-sumStress.sumx* sumStress.sumx)*(stressLength*sumStress.sumyy-sumStress.sumy*sumStress.sumy))
                // const result = regression;

                let stressLinearRegressionTrace = {
                    x: ['1분', '2분', '3분', '4분', '5분', '6분', '7분', '8분', '9분', '10분', '11분', '12분', '13분', '14분', '15분'],
                    y: stressLinearRegressionData,
                    name: 'regression',
                    mode: 'lines',
                    line:{
                        dash:'dashdot',
                        width:2,
                    }
                }

                document.querySelector("#stressMaxTime").textContent = maxStress.minute;
                document.querySelector("#stressMinTime").textContent = minStress.minute;
                document.querySelector("#stressCorrelation").textContent = (stressCoefficient  <0?'Negative':'Positive')
                    +"("+ stressCoefficient.toFixed(2) +")";



                let stressPowerChartData = baselineTrace ? [stressAvg,stressLinearRegressionTrace,baselineTrace]:  [stressAvg,stressLinearRegressionTrace];
                Plotly.newPlot('stressPowerChart', stressPowerChartData, doubleChartWidthLayout, config);
            });

    }

    $(document).ready(function () {
        showAllChart();

        // let aggMhr = JSON.parse($('#aggMhr')[0].dataset.aggmhr);
        // let allMhrLog = JSON.parse($('#allMhrLog')[0].dataset.allmhrlog);


    }); // end of document ready


    const delay = (ms) => {
        var start = new Date().getTime();
        var end = start;
        while (end < start + ms) {
            end = new Date().getTime();
        }
    };

    const sleep = ms => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

})
(jQuery, jStat); // End of use strict
