const students = (function ($) {
    "use strict"; // Start of use strict
    const remote_location = 'https://mave33.cafe24.com/mave_griffin/branches/';
    const branch_idx = document.querySelector('#branch_idx').value;
    const branch_name = document.querySelector('#branch_name').value;
    const student_types = {'중학생': "primary", "고등학생": "success", "일반": "info"};
    const report_types = {'inprogress': 'success', 'report': 'primary', 'inactive': 'secondary'}

    const fetchData = function (url, nextPage) {
        let requestUrl = remote_location + branch_idx + "/students/all";
        return fetch(requestUrl)
            .then(res => {
                    return res.json();
                }
            );
    }

    const renderStudents = function (students) {
        if (students && students.length > 0) {
            students.map(student => {

                let textColor;//  = student_types[student.student_type];
                let link;
                let liveStreamOpenTimeLimit = 60; // seconds

                if ( student.mh_status === "DONE"){
                    link = ` <a href="#" class="stretched-link"
                                 data-toggle="modal" data-target="#historyModal"
                                 data-student_name = "${student.student_name}"
                                 data-student_idx = "${student.student_idx}"></a> `;
                    textColor = report_types['report'];
                }else if( student.mh_status ==="PROG"){
                    link = student.start_time_diff> liveStreamOpenTimeLimit
                        ?  `<a href="branches/${branch_name}/students/${student.student_idx}/monitor" class="stretched-link"></a> `
                        :`<a href="#" class="stretched-link" data-toggle="modal" data-target="#waitingForLiveStream"></a>`
                    textColor = report_types['inprogress'];
                }else{
                    link = `
                    <a href="#" class="stretched-link" data-toggle="modal" data-target="#inactiveStudent"></a>
                    `;
                    textColor = report_types['inactive'];
                }
                let last_tested = student.created_at?"<i class=\"fas fa-clock\"></i> "+student.created_at:'';


                let frag = `
                      <div class="col-xl-3 col-md-6 mb-4 pl-0">
                        <div class="card border-left-${textColor} shadow h-100 py-2">
                            <div class="card-body" data-student_idx ="${student.student_idx}">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-${textColor} text-uppercase mb-1">${student.student_type}</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">${student.student_name}</div>
                                        <div class="h6 mb-0 font-weight text-gray-1000">${last_tested}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-user fa-2x text-${textColor}"></i>
                                    </div>
                                </div>
                                ${link}
                            </div>
                        </div>
                    </div>
                    `;
                $('.student-row').append(frag);
            });
        } else {
            //   student
        }
    }

    const fetchDataMh = function (student_idx) {
        let requestUrl = "https://mave33.cafe24.com/mave_griffin/mh/" + student_idx + "/all";
        return fetch(requestUrl)
            .then(res => {
                    console.log('fetch data');
                    console.log(res);
                    return res.json();
                }
            );
    }

    function getStudentHistory(student_idx) {

        fetchDataMh(student_idx)
            .then(res => {
                if (res && res.status === 'success') {
                    res.message.result;
                    return res.message.result;
                } else {
                    console.log('failed');
                }
            }).then(list => {
            renderStudentHistory(list);
        });
    }

    let  datatable ;
    /**
     *
     * @param historyList [ { branch_name:  xxxx, student_idx : dd , meditation_history_idx : dd , }
     */
    function renderStudentHistory(historyList) {
        $("#mh_list").empty();
        // console.log(historyList);
        /*
        historyList.map((row,index) => {
            let date = moment(row.created_at).format("YYYY-MM-DD hh:mm:ss");
            let iteration = index +1;
            let resource = row.resource;
            let fragment = `
             <tr style="transform:rotate(0);">  
                <th scope="row">
                    <a href="branches/${branch_name}/students/${row.student_idx}/report/${row.meditation_history_idx}" class="stretched-link">
                        ${iteration}
                    </a>
                </th>
                <td>${resource}</td>
                <td>${date}</td>
             </tr>
           `;
            $("#mh_list").append(fragment);
        }).reduce(res=>{
            console.log('tables done')
            $("#studentHistoryTable").dataTable();
        });
*/

        let index =0;
        if ( $.fn.dataTable.isDataTable( '#studentHistoryTable' ) ) {

            datatable.clear().rows.add(historyList).draw();
        }
        else {
            datatable =
                $("#studentHistoryTable").DataTable({
                    data:historyList,
                        retrieve:true,
                    ordering: false,
                    searching: false,
                    lengthChange: false,
                    language: {
                        info: "총 _PAGES_ 페이지 중 현재 _PAGE_ 페이지  ",
                        infoEmpty:" 회차 정보가 없습니다.",
                        emptyTable : '불러올 정보가 없습니다.'
                    },
                        columns: [
                            {data:null, render:function(data,type,row, meta){
                               return ` <a href="branches/${branch_name}/students/${data.student_idx}/report/${data.meditation_history_idx}" >
                                        ${meta.row+1}
                                        </a>`;
                                }
                            },
                            {data: 'resource'},
                            {data: 'created_at'},
                        ],
                    }
                );
        }



    }


    $("#historyModal").on('show.bs.modal', function (event) {
        let link = $(event.relatedTarget);
        let student_idx = link.data('student_idx');
        let student_name = link.data('student_name');
        $("#tableStudentName").text(student_name);
        getStudentHistory(student_idx);


    });

    $("#reloadingBtn").on('click', function(e){
        e.preventDefault();
        location.reload();
    });

    $(document).ready(function () {
        fetchData('All').then(res => {
            if (res && res.status === 'success') {
                $(".student-row").empty();
                renderStudents(res.message);
            } else {
                console.log(' fail ');
            }
        });
    });

    return {
        getStudentHistory: function (student_idx) {
            getStudentHistory(student_idx);
        }
    };


})
(jQuery); // End of use strict